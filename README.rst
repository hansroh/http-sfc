Single File Component Based Website
==========================================

`FranckFreiburger/http-vue-loader`_ Based Website Template:
Vue.js + Python Implementation

- No node.js, No webpack, No build
- `Vue SFC`_ (Single File Component) Based Web Developing

I implemented with VueJS + Python, but it is more
like website developement pattern not library nor
framework belong to specific programming languages.

Javascript

- `Babel Standalone`_ (Transpiler)
- `Vue.js`_ + Vuex_ + `Vue Router`_ (Fronrend framework)
- `Element UI`_ (UI framework)
- `Overthrow`_ (CSS overflow native-like scrolling helper)

Python

- Jinja2_: Serverside Template Engine
- Atila_: WSGI App Framework
- Skitai_: WSGI/Web Server


But Why?
------------

For building website NOT webapp.

Maybe, building webapp need node and webpack. And I think website
is way differnet from webapp.

Website generally has some features.

- URL which can bookmark and copy and send it
- Contents can be readable for both human and machines (crawlers)

  There are lots of crawlers which cannot render Javascript generated
  contents. If your contents is written as plain HTML, you have more
  chances to be crawled, indexed, serached by web search engine.

- UI/UX also very important matter for human

  - Browser's back/forward/reload buttons must work properly
  - Reactive UI/UX

But, I think recent frontend frameworks are not fit for these
requirements or too complex to achieve.

Then I'm looking into this matter and it is ongoing progress.

I think what if 'Content Driven Website'. Simply focus on contents
(or pages) which are requested 'GET' method.

A. Haman and crawlers have interest in READING.
B. Human (including AI) also has interest in MUTATIONS -
   searching, filtering, editing, deleting, chaning, posting
   new one...

In traditional website, cost of B is somewhat expensive. Every
mutation actions need reloading entire page. and some solutions
are sudgested like <frame>, AJAX for reducing network/system load.

Modern frontend frameworks largely use AJAX feature and focus
on `state` of data not `event` nor `element handling`. And it
makes improvement of UX/UI and reached modern SPA (Single Page
App).

Anyway, what solutions is for getting both A and B?

1. get simple HTML contents for human and crawlers.

   this can fix many problems which cause by SPA like
   giving proper HTTP status codes, non script
   browser/crawler's readability and so on.

2. use UI/AJAX for mutating that reduce network/system load

Shortly,

GET HTML and Mutate (POST, PUT, DELETE and PATCH) with
modern UI frameworks/AJAX.

This project uses:

- Jinja2 template engine for A and communicating with B
- VueJS and Element UI for B
- For efficient B, used `http-vue-loader` for UI component
  base development
- And for small scaled B that need not crawler friendly,
  `http-vue-loader` based SPA example is also included.

  in my context, SPA is not accuracy, `single page feature`
  is more proper, I think. it is more complex and larger than
  `single page contents including mutations`.

And now, we have `http-vue-loader`_. In fact, this is my dream of
decades comes true. HTML including, and rendered on client with
scoped javascript and style with global state management.

I think this is the one of most revolutionary one in HTML/JS
world for decades. Even better, it is works fine with legacy
web standards perfectly. We just add 2 or more <script>
tags (amazing!).


**Conclusion:**

I suggest belows for building website:

- compose with `contents` or `feature` focused mirco sub apps
- generate html head and page header on server side for cralwer
- generate core contents for human and crawlers on server side
  if you need
- make SFCs for UI parts, and they will be cached and rendered
  on client side
- each sub apps may have own router if you need


Installation
----------------------

System Requirements:

- Python 3.5+

.. code:: bash

  git clone https://gitlab.com/hansroh/http-sfc
  cd http-sfc
  pip3 install -Ur requirements.txt


Running Server
--------------------

.. code:: bash

  ./serve.py --devel

Go to http://localhost:5000 on your browser (Safari, Chrome or
Edge, Maybe not MSIE).

Note: In production mode, DO NOT USE `--devel` option.


Template 1 (Without Router)
-----------------------------------

Search engine friendly MPA (Multi Page App) with CDN/<script> tag.

- Cooporative Rendering: render core data and links on
  server-side, UX components on client-side
- `Element UI`_ + Jinja2_ + Atila_

Shortly, server has reponsibility for representing important contents
data which will be used client UX:

- render core content which must be exposed to web crawler
- generate text links, images which can be identified by crawler
- inject data which need handling DOM object using `data-` attribute
- write root Vue for mainly loading injected data automatically

And client has reponsibility for UX components:

- root Vue load data to Vuex store automatically
- load UX .vue files with `http-vue-loader`

`Cooperative Rendering` is built by Jinja2 template_
and mainly has 3 major parts.


HTML
``````````````

Write HTML with important data to be exposed. Data is not just
content human readable but also page meta data like HTML headers.

Basic principles:

- Just care about data which crawlers care.
- Place Vue component tags related human UX - meaning
  POST, PUT, DELETE, PATCH related or form/button related.
- If you want VueJS's double mustache on content part, it could
  become someting wrong. You probably use only vuejs' directives
  for representing content's state change for human.

.. code:: jinja

  <!-- displaying stat component -->
  <display-stat type-filter></display-stat>

  {% for row in context.rows %}
    <!-- state data which can be mutable is contained with 'data-' attribute -->
    <el-card
      :class="(rows [{{ loop.index0 }}].status != 'todo' ) ? 'dim':''"
      data-id="{{ row.id }}"
      data-status="{{ row.status or '' }}"
      data-text="{{ row.text and '' }}"
    >
      <el-row>
        <el-col :xs=2 :sm=1 :md=1>
          <b>{{ loop.index + ((request.ARGS.get ('page', 1) - 1) * limit) }}</b>
        </el-col>

        <el-col :xs=22 :sm=14 :md=13>
          {{ row.text }}
        </el-col>

        <el-col :xs=24 :sm=9 :md=10>
          <!-- status changing component -->
          <change-status :rownum='{{ loop.index0 }}'></change-status>
        </el-col>
      </el-row>
    </el-card>
  {% endfor %}


Mapping Vuex State Data
```````````````````````````````````

Inject state data into Vuex. This make Python object to
Vuex data by converting JSON format internally.

Actually this is the most important process to transit
from backend data to frontend for mutating state.

In the block state_map, you can use 2 Jinja2 template macros:

- map_data (name, default, container = '', list_size = 0):
  convert Python primitive objects to Javascript objects.

  - name: name of Vuex state
  - default: initial default data, data must be JSON
    serializable
  - container: optional CSS, real data containing element.

    If you have multiple array like data and need to bind
    each of them to Vue, you may have same length of JS
    array before Vue mounts app element.

    At this case, `default` must be one element of array
    with default values.

- list_size: length of container (meaning data length)
- map_dict (name, \*\*kwargs)

  Same as map_data except keyword argument way.

- map_route (\*\*kwargs)

  Given keyword arguments will be rendered to URL spec
  which will be used Vue.$urlfor ()


.. code:: jinja

  {% block state_map %}
    ## route rules
    {{ map_route (pagingUrl = "todos.index", updateUrl = "todos.item") }}

    ## simple data
    {{ map_data ('context', context | upselect ('record_count', 'limit', current_page = 1)) }}

    ## dict by keyword arguments style
    {{ map_dict ('types', todo = 0, cancneled = 1, done = 2) }}

    ## DOM containing array data
    {{ map_data ('rows', { "id": 0, "status": 'todo', "text": '' }, ".el-card", context.limit) }}

  {% endblock %}


Belows are automatically mapped.

- $args: URL parameters and query data, this is automatically
  created
- $debug

It is recommended below data:

- context: context dict related current URL
- views: UI view states

  Note: `$routes` must be created by map_dict
  ('$routes', name = name of resource, ...)

Also additional template filter upselect will be
useful for handling dict data.

- upselect (dict val, \*names, \*\*upserts): create new dict from
  given dict by select key or upsert data

.. code:: jinja

  context = {'a': 1, 'b': 2, 'c': 3}
  {{ context | upselect ('a', b = 10, d = 4) }}
  # {'a': 1, 'b': 10, 'd': 4}

As a result, it will create kind of code making Vuex object,

.. code:: js

  cost store = new Vuex ({
    state: {
      args: {page: 1},
      routes: {pagingUrl: '/page1', updateUrl: 'page2'},
      context: {record_count: 100, limit: 10, current_page = 1}
    },
  })

These can be used by call global function mapVuexItems ()
in your Vue.computed.

.. code:: html

  computed: {
    ...mapVuexItems (),
  },

*Note:*

Vuex's conventianl usage is state management for large scale app,
but in this case, Vuex is more like as data container which can be
accessed by all Vue components. For simplicity, Vuex muations
and actions are not essential for now.

But for using these features, you can write Jinja2 blocks,

- vuex_getters
- vuex_muations
- vuex_actions


Vue Components
``````````````````````````

Finally load vue components which is used HTML part.

.. code:: jinja

  {% block vue_components %}
    {{ component ('todos/display-stat.vue') }}
    {{ component ('todos/change-status.vue') }}
  {% endblock %}

`component ()` macro also helps HTTP/2 or HTTP/3 server pushing.

And your vue file is like this (please, check out *<script lang="babel">*),

.. code:: html

  <!-- /static/components/todos/display-stat.vue -->

  <template>
    <div>
      <b>Data Status: {{ context.record_count }}</b>
    </div>
  </template>

  <script lang="babel">
    export default {
      computed: {
        /* it will map all vuex state data
           which are injected by Jinja2 template. */
        ...mapVuexItems (),
      },
    }
  </script>


Template 2 (With Router)
--------------------------------------

This is inspired from `homomaid.vue`_. For more detail
visit homepage.

This template maybe useful on these situations:

- human and crawler readability of your new feature
  is not important
- personalized content that never exposed to search
  engines and app-like UI/UX is more important

Basically same as `Template 1` except you should
give  `route_base` in template context for activating
Vue-Router.

.. code:: python

  @app.route ("/<path:path>", methods = ["GET"])
  def index (was, path = None):
      return was.render ('pages/router/index.j2',
          route_base = was.baseurl (index)
      )

And based on `route_base` and vue files of
`/static/pages`, routes will be generated automatically.

For example,

.. code:: js

  {name: "/", path: "/", component: httpVueLoader ("/pages/router/index.vue")},
  {name: "/sub/:id", path: "/sub/:id", component: httpVueLoader ("/pages/router/sub/_id.vue")}
  // this is same as above
  {name: "/sub/:id", path: "/sub/:id", component: httpVueLoader ("/pages/router/sub/_id/index.vue")}

It is similar as `Nuxt.js` way.

*Note:* In this case, HTTP/2 or HTTP/3 server pushing feature will
not be enabled EXCEPT index page. But you can enable manullay for
each individual sub pages.

You can access this page from http://localhost:5000/router
after starting server.


Helpers List
-------------------------------

Note: Some of these are related `Element UI`_. If you doesn't
want to use it, you need some modifications.


Jinja2 Macros
`````````````````````````````

- csrf (): create *<meta name="csrf" content="3db6b9cc53c8692c">*
- map_route (): *see above*
- map_data (): *see above*
- map_dict (): *see above*
- map_text (name, container)
- map_html (name, container)
- set_cloak (flag = True): cloak entire page on loading
- make_pagenation (page_param, total, page_size, page_url,
  pager_count = 5)
- component (path, alias)
- global_component (path, alias)

Jinja2 Filters
``````````````````````````

- upselect
- tojson_with_datetime
- typemap
- vue (str vue_statement)

Jinja2 Global Functions
`````````````````````````````

- raise (str error_msg)

Vuex Map Helper
````````````````````````

- mapVuexItems (): use in computed

Vuex Default States
````````````````````````

- $args: URL args with default value
- $debug: sync with app.debug
- $csrf: csrf token
- $user: user info in session

  - uid
  - lev
  - nick_name
  - status

- $ls: local storage helper

  - set (name, data, timeout = 0) // 0 means never expired
  - get (name): null if not cached
  - remove (name)
  - clear () // clear all data

- $ss: session storage helper


Vue/Vuex Helper Methods
`````````````````````````````

In Vuex, you can access by like window.app.$urlfor () or
window.app.$unotify ().

- $urlfor (name, args = [] or {}, kargs = {}): build URL with map_route ()
  with $store.state.routes
- $traceback (e):
- $set_cloak (flag)
- $load_script (src, callback = () => {}): load CDN script in SFC
- $get_ref_from_parent (ref): get parent $refs.ref

And user interaction methods using `Element UI`_, but you can
implement with any UI library.

- $nnotify (title, message, icon): native notifification
- $uloading (text = null, opacity = 0.7)
- $unotice (title, message, type = 'info', duration = 3000)
- $usnackbar (message, type = 'info', duration = 3000)
- $ualert (title, message, callback = (v => {}))
- async $uconfirm (title, message, type = 'warning')
- async $uprompt (title, message, inputPattern = null, inputErrorMessage = null)
- $viewport (): return xs|sm|md|lg|xl
- $device

  - android ()
  - ios ()
  - mobile ()
  - touchable ()
  - rotatable ()

- $ut

  - date ("2020-03-09 01:01:36"): input timezone is should be UTC, return Date object


Websocket Methods
```````````````````````````````````

Inject in Vue methods:

.. code:: js

  methods: {
    ...websocketMethods ()
  },

- $wconnect (url, read_handler = (evt) => this.$log (evt.data))
- $wpush (msg)
- $wclose ()

Style Supplements
`````````````````````````````

Addtional helpful styles mostly from `Vuetify.js`_.

See `supplement.css`_.


.. _`FranckFreiburger/http-vue-loader`: https://github.com/FranckFreiburger/http-vue-loader
.. _`http-vue-loader`: https://github.com/FranckFreiburger/http-vue-loader
.. _`homomaid.vue`: https://github.com/chitoku-k/homomaid.vue
.. _template: https://gitlab.com/hansroh/vuepy/blob/master/vuepy/templates/pages/todos/index.j2

.. _`Element UI`: https://element.eleme.io/#/en-US
.. _Jinja2: https://jinja.palletsprojects.com/en/2.10.x/templates/
.. _Atila: https://pypi.org/project/atila/
.. _Skitai: https://pypi.org/project/skitai/
.. _Babel: https://github.com/FranckFreiburger/http-vue-loader/issues/89
.. _`Babel Standalone`: https://babeljs.io/docs/en/babel-standalone

.. _`Vue.js`: https://vuejs.org/
.. _Vuex: https://vuex.vuejs.org/
.. _`Vue Router`: https://router.vuejs.org/
.. _`Vue SFC`: https://vuejs.org/v2/guide/single-file-components.html
.. _`Overthrow`: https://github.com/filamentgroup/Overthrow
.. _`Vuetify.js`: https://vuetifyjs.com/
.. _`supplement.css`: https://gitlab.com/hansroh/http-sfc/blob/master/http-sfc/static/libs/site/supplement.css
