if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('/sw.js').then(function(registration) {
            console.log('[sw-init.js] registration successful with scope', registration.scope)
            registration.addEventListener('updatefound', () => {
                const newWorker = registration.installing
                console.log('[sw-init.js] service worker update found')
                newWorker.addEventListener('statechange', () => {
                    console.log('[sw-init.js] service worker state changed', newWorker.state)
                })
            })
        }, function(err) {
            console.log('[sw-init.js] registration failed', err)
        })

        navigator.serviceWorker.addEventListener ('controllerchange', () => {
            console.log('[sw-init.js] controller changed')
        })

    })
}