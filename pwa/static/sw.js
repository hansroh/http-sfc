// caching -----------------------------------------
const DEBUG = true
const USE_CACHE = false
const PRE_CACHE_NAME = 'pre-cache-v1'
const DYN_CACHE_NAME = 'dyn-cache-v1'

// config ---------------------------------------------
let URL_TO_CACHE = [ // cache html files for app display
  '/offline.html',
  '/manifest.json',
]

let DYN_CACHE = {
  enable: true,
  pathes: [ //cache by path
    /\.(svg|jpeg|jpg|png|gif|ico)$/i,
    /\.vue$/i,
    /\.(woff|woff2|ttf)$/i,
    /\.(js|css)$/i,
  ],
  mimetypes: []
}

let DYN_NOCACHE = {
  enable: true,
  pathes: [ //no cache by path
    /\/sw\.js$/i,
    /\/sw-init\.js$/i,
  ],
  mimetypes: []
}

let API_PAYLOAD_CACHE = {
  enable: false,
  pathes: [],
  mimetypes: [
    'application/json',
  ]
}

let CORS_CACHE = {
  enable: true,
  pathes: [
    /\.(woff|woff2|ttf)$/i,
    /\.(js|css)$/i,
  ],
  mimetypes: []
}

// end of config --------------------------------------

function matched (config, url, content_type) {
  if (config.enable === false) return false
  if (config.pathes.filter (re => url.match (re)).length) return true
  if (config.mimetypes.filter (mtype => content_type.startsWith (mtype)).length) return true
  return false
}

function log (msg) {
  if (DEBUG) {
    console.log(`[sw.js] ${ msg }`)
  }
}

if (API_PAYLOAD_CACHE.enable) {
  DYN_CACHE.mimetypes = DYN_CACHE.mimetypes.concat (API_PAYLOAD_CACHE.mimetypes)
}

self.addEventListener('activate', function(event) {
  log ('activated')
  let CACHES_TO_PRESERVE = []
  if (USE_CACHE) {
    CACHES_TO_PRESERVE = [PRE_CACHE_NAME, DYN_CACHE_NAME]
  }
  event.waitUntil(
    caches.keys().then(function(cacheList) {
      return Promise.all(
        cacheList.map(function(cacheName) {
          if (CACHES_TO_PRESERVE.indexOf(cacheName) === -1) {
            log (`cache deleted ${ cacheName }`)
            return caches.delete(cacheName)
          }
        })
      )
    }).catch(function(error) {
      log (`cache deletion error ${ error }`)
    })
  )
})

if (USE_CACHE) {
  self.addEventListener('install', function(event) {
    if (DEBUG) self.skipWaiting()
    event.waitUntil(
      caches.open(PRE_CACHE_NAME).then(function(cache) {
        log (`cache opened: ${PRE_CACHE_NAME}`)
        return cache.addAll(URL_TO_CACHE)
      })
    )
  })

  self.addEventListener('fetch', function(event) {
    let cached_content = null
    event.respondWith(
      caches.match(event.request).then(function(r) {
        const url = event.request.url
        let content_type = ''
        if (r) {
          content_type = r.headers.get ('content-type') || ''
          if (!matched (API_PAYLOAD_CACHE, url, content_type)) {
            log (`cache hit ${url}`)
            return r
          } else {
            cached_content = r
            log (`refreshable cache ${url}`)
          }
        }

        const fetchRequest = event.request.clone()
        return fetch (fetchRequest)
          .then(function(response) {
            if (fetchRequest.method !== 'GET') {
              return response
            }

            if(!response || response.status !== 200) {
              return response
            }

            if (response.type !== 'basic' && !matched (CORS_CACHE, url, content_type)) {
              return response
            }

            content_type = response.headers.get ('content-type') || ''
            if (matched (DYN_NOCACHE, url, content_type)) {
              log (`skip caching ${url}`)
              return response
            }

            if (matched (DYN_CACHE, url, content_type)) {
              return caches.open(DYN_CACHE_NAME).then(function(cache) {
                log (`caching ${url}`)
                cache.put(fetchRequest, response.clone ())
                return response
              })
            }

            return response
          })
        })

        .catch (async function(error) {
          if (cached_content !== null) {
            log ('cache fetch error, reuse cached data')
            return cached_content
          }
          log ('cache fetch error')
          return caches.open (PRE_CACHE_NAME).then (function(cache) {
            if (event.request.headers.get ('accept').includes ('text/html')) {
              return cache.match ('/offline.html')
            }
          })
        })
    )
  })
}

// message pushing -------------------------------------
self.addEventListener('push', event => {
  log (`push ${ event.data.text() }`)
  const title = 'My PWA!';
  const options = {
    body: event.data.text(),
    icon: '/assets/favicons/favicon-128.png'
  }
  event.waitUntil(self.registration.showNotification(title, options));
})

self.addEventListener('notificationclick', function(event) {
  log ('push clicked')
  event.notification.close()
  event.waitUntil(
    clients.openWindow('/')
  )
})

// sync --------------------------------------------------
const SYNC_METHODS = {
  testSync () {
    fetch ('/favicon.ico')
      .then (response => {
        return response
      })
      .then (text => {
        log (`request successful ${ text }`)
      })
      .catch (error => {
        log (`request failed ${ error }`)
      })
  },
}

self.addEventListener ('sync', event => {
  if (event.tag == 'testSync') {
    event.waitUntil (SYNC_METHODS [event.tag] ())
  }
})
