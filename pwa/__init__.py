import os
import skitai
import json

def __setup__ (pref):
    pref.config.SERVICE_WORKER = True
    pref.config.FRONTEND_CONFIG = {}
    pref.config.MINIFY_HTML = 'strip'

    cred_file = os.path.join (os.path.dirname (__file__), 'resources', 'etc', 'google-credential.json')
    if os.path.exists (cred_file):
        import firebase_admin
        from firebase_admin import credentials
        cred = credentials.Certificate (cred_file)
        firebase_admin.initialize_app (cred)

        cred_file = os.path.join (os.path.dirname (__file__), 'resources', 'etc', 'frontend-config.json')
        if os.path.exists (cred_file):
            with open (cred_file) as f:
                pref.config.FRONTEND_CONFIG = json.load (f)

    # get key from https://web-push-codelab.glitch.me/
    cred_file = os.path.join (os.path.dirname (__file__), 'resources', 'etc', 'push-app-server-key.json')
    if os.path.exists (cred_file):
        with open (cred_file) as f:
            pref.config.PUSH_API_SERVER_KEY = json.load (f)

    if 'TIMEOUT_LTS' not in pref.config:
        pref.config.TIMEOUT_LTS = 14400 * 30
        pref.config.TIMEOUT_STS = 3600 * 6

    if 'DEFAULT_MAX_AGE' not in pref.config:
        pref.config.DEFAULT_MAX_AGE = 7200

    skitai.set_max_age ("/libs", 14400)
    skitai.set_max_age ("/components", pref.config.DEFAULT_MAX_AGE)
    skitai.set_max_age ("/assets", pref.config.DEFAULT_MAX_AGE)
    skitai.set_max_age ("/pages", pref.config.DEFAULT_MAX_AGE)
    skitai.set_worker_critical_point (interval = 60)
