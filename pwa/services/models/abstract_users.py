
from skitai.corequest.tasks import Mask
from rs4.attrdict import AttrDict
from datetime import datetime

class Users:
    rows = {}
    @classmethod
    def get (cls, uid = None, nick_name = None):
        assert uid or nick_name
        user = None
        if uid:
            try: user = cls.rows [uid]
            except KeyError: pass
        else:
            for user_ in cls.rows.values ():
                if nick_name == user_.nick_name:
                    user = user_
                    break
        return Mask ([user]) if user else Mask ([])
    get_profile = get

    @classmethod
    def add (cls, uid, payload = None):
        defaults = dict (
            nick_name = '',
            lev = 'user',
            status = None,
            email_verified = False
        )
        payload = payload or {}
        payload ['idx'] = len (cls.rows) + 1
        defaults.update (payload)
        user = cls.rows [uid] = AttrDict (dict (
            uid = uid,
            last_updated = datetime.now (),
            **defaults
        ))
        return Mask ([user])

    @classmethod
    def update (cls, uid, payload):
        user = cls.rows [uid]
        user.update (payload)
        return Mask ([user])

    @classmethod
    def delete (cls, uid):
        del cls.rows [uid]
        return Mask ([])

    @classmethod
    def set_permission (cls, uid, payload):
        return cls.update (uid, payload)

