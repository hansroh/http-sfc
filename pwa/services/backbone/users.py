try:
    from ..models.users import Users
except ImportError:
    from ..models.abstract_users import Users
from .auth import AUTH_SESSION_NAME
import random


def __mount__ (app):
    @app.route ('/<uid>/profile', methods = ['GET'])
    @app.permission_required (uid = ['staff'])
    def get_profile (was, uid):
        return was.API (profile = Users.get (was.request.user.tuid).one ())

    @app.route ('/<uid>/open_profile', methods = ['GET'])
    def get_open_profile (was, uid):
        return was.API (profile = Users.get_profile (uid).one ())

    @app.route ('/<uid>/profile', methods = ['DELETE', 'OPTIONS'])
    @app.permission_required (uid = ['staff'], booleans = ['real', 'cancel'])
    def delete_profile (was, uid, cancel = False, real = False):
        if real:
            Users.delete (was.request.user.tuid).commit ()
        else:
            Users.update (was.request.user.tuid, {'status': not cancel and 'resigned' or None}).commit ()
            if cancel:
                was.session.remove ('status')
            else:
                was.session.set ('status', 'resigned')
            uid == 'me' and was.broadcast ('resign', was.request.user.uid, cancel)
        return was.API ("201 No Content")

    UNMODIFIABLES = {'idx', 'uid', 'lev', 'status'}
    @app.route ('/<uid>/profile', methods = ['PATCH', 'OPTIONS'])
    @app.permission_required (['owner'])
    def patch_profile (was, uid, **payload):
        if not was.request.user.nick_name and 'maybe_nick_name' in payload:
            maybe_nick_name = payload.pop ('maybe_nick_name')
            #if not maybe_nick_name:
            #    maybe_nick_name = payload.get ('email', '').split ('@')[0]
            if maybe_nick_name:
                users = Users.get (nick_name = maybe_nick_name).fetch ()
                if not users:
                    payload ['nick_name'] = maybe_nick_name
                else:
                    dups = { each.nick_name for each in users }
                    for i in range (10):
                        temp = '{}#{}'.format (maybe_nick_name, random.randrange (9999))
                        if temp not in dups:
                            payload ['nick_name'] = temp
                            break

        if payload.get ('nick_name'):
            if len (payload ['nick_name'].encode ()) > 24:
                raise was.Error ("400 Bad Request", 'too long nickname')

            if payload ['nick_name'] and payload ['nick_name'] != was.request.user.nick_name:
                user = Users.get (nick_name = payload ['nick_name']).fetch ()
                if user:
                    payload ['nick_name'] = None
                elif uid == 'me':
                    was.session.mount (AUTH_SESSION_NAME, was.app.config.TIMEOUT_LTS, extend = False)
                    if was.session.get ('nick_name') != payload ['nick_name']:
                        was.session.set ('nick_name', payload ['nick_name'])
            else:
                payload.pop ('nick_name')


        for each in UNMODIFIABLES:
            if each in payload:
                raise was.Error ('403 Permission Denied', 'you have not permission for this operation')

        for k, v in payload.items ():
            if v == '':
                payload [k] = None

        email_verified = payload.get ('email_verified', False)
        if payload.get ("provider") == 'password' and not email_verified:
            payload ['status'] = 'unverified'
            was.session.set ('status', 'unverified')
        if email_verified:
            payload ['status'] = None
            was.session.remove ('status')

        Users.update (was.request.user.tuid, payload).commit ()
        uid == 'me' and was.broadcast ('profile-changed', was.request.user.uid, payload)
        return was.API ("200 OK", new_nick_name = was.session.get ('nick_name'))

    @app.route ("/<uid>/permission", methods = ["PATCH", "OPTIONS"])
    @app.permission_required (['staff'])
    def set_permission (was, uid, status, lev = None):
        assert status != 'resigned', 'unacceptable status'
        was.broadcast ("status-changed", was.request.user.tuid, status)
        payload = {'status': status}
        if lev: payload ['lev'] = lev
        Users.set_permission (was.request.user.tuid, payload).commit ()
        return was.API ("200 OK")
