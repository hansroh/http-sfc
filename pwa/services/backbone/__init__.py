# mainly supporting HTTP-SFC
# Created on Feb 12, 2020 by Hans Roh (hansroh@gmail.com)

import os
import json
from skitai.wastuff.api import ISODateTimeWithOffsetEncoder
import re
from . import auth, users

def __setup__ (app, mntopt):
    app.mount ('/auth', auth, return_url = '/')
    app.mount ('/users', users)

RX_SPACE = re.compile (r'\s+')
ROUTES_CACHE = {}
def __mount__ (app):
    # template globals ------------------------------------
    @app.template_global ('raise')
    def raise_helper (was, msg):
        raise Exception (msg)

    @app.template_global ('build_routes')
    def build_routes (was, base):
        def collect (start, base_len):
            pathes = []
            for fn in os.listdir (start):
                path = os.path.join (start, fn)
                if os.path.isdir (path):
                    pathes.extend (collect (path, base_len))
                elif fn.endswith ('.vue'):
                    pathes.append ((
                        path [base_len:-4].replace ("\\", '/').replace ('/_', '/:'),
                        path [base_len:].replace ("\\", '/'),
                    ))
            return pathes
        global ROUTES_CACHE

        if not app.debug and base in ROUTES_CACHE:
            return ROUTES_CACHE [base]
        root = os.path.join (app.home, 'static', 'pages', base [1:])
        routes = []
        pathes = []
        for path, vue in collect (root, len (root)):
            if path.endswith ('/index'):
                path = path [:-6] or '/'
            vue_path = "/pages{}{}"    .format (base, vue)
            routes.append ('{name: "%s", path: "%s", component: httpVueLoader ("%s")}' % (path [1:] or 'index', path, vue_path))
            if path == '/':
                was.push (vue_path) # server push
            else:
                pathes.append (vue_path) # prefetch
        routes_list = ", ".join (routes)
        ROUTES_CACHE [base] = (routes_list, pathes)
        return routes_list, pathes

    # template filters --------------------------------------
    @app.template_filter ('vue')
    def vue (val):
        return '{{ %s }}' % val

    @app.template_filter ('summarize')
    def summarize (val, chars = 60):
        if not val:
            return ''
        s = val.find (" ", chars)
        if s == -1:
            return RX_SPACE.sub (" ", val.strip ())
        else:
            return RX_SPACE.sub (" ", val.strip () [: min (s, chars + 10)]) + '...'

    @app.template_filter ('attr')
    def attr (val):
        if not val:
            return '""'
        return '"{}"'.format (val.replace ('"', '\\"'))

    @app.template_filter ('upselect')
    def upselect (val, *names, **upserts):
        d = {}
        for k, v in val.items ():
            if k in names:
                d [k] = v
        d.update (upserts)
        return d

    @app.template_filter ('tojson_with_datetime')
    def tojson_with_datetime (data):
        return json.dumps (data, cls = ISODateTimeWithOffsetEncoder, ensure_ascii = False)

