from . import websocket, todos, router, index
import json

def __setup__ (app, mntopt):
    app.mount ('/', index)
    app.mount ('/todos', todos)
    app.mount ('/router', router)
    app.mount ('/websocket', websocket)

    @app.on_broadcast ('login')
    def on_login (was, uid, is_member):
        was.log ('{} login'.format (uid))

    @app.on_broadcast ('logout')
    def on_logout (was, uid):
        was.log ('{} logout'.format (uid))

    @app.on_broadcast ('profile-changed')
    def on_profile_update (was, uid, payload):
        was.log ('{} profile updated with {}'.format (uid), payload)

    @app.on_broadcast ('resign')
    def on_profile_update (was, uid, cancel = False):
        was.log ('{} resign {}'.format (uid, cancel and 'canceled' or 'requested'))
