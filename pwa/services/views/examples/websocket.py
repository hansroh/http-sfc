import skitai
from ...helpers import jobs
import psutil

def onopen (was):
    jobs.attach (was.request.ARGS ["userid"], was.websocket)

def onclose (was):
    jobs.dettach (was.request.ARGS ["userid"])

# app decorating ---------------------------------------------------

def mount (app):
    @app.route ("")
    @app.websocket_config (skitai.WS_CHANNEL | skitai.WS_THREADSAFE, 300, onopen, onclose)
    def websocket (was, message, userid):
        if message.startswith ("run-mkclip "):
            if jobs.has_job (userid):
                return "error|You already have job"
            params = message.split (" ")[1]
            id, clips = params.split ("|")
            job = jobs.VideoClipping (userid, was.websocket, was.app.config.MKCLIP, id, clips)
            job.start ()
            jobs.add_job (userid, job)

        elif message ==  "cpu":
            return ">> cpu {:.1f}".format (psutil.cpu_percent ())

        elif message ==  "kill process":
            jobs.kill (userid)
            return 'killed'

        return '{} said: {}'.format (userid, message)
