def __mount__ (app):
    @app.route ("/<path:path>", methods = ["GET"])
    def index (was, path = None):
        return was.render ('pages/examples/router/index.j2',
            route_base = was.baseurl (index),
            layout = '4piece'
        )

    # APIs ---------------------------------------------------
