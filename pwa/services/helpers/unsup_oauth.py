import hashlib
import base64

PROVIDERS = {
    'naver': 'https://openapi.naver.com/v1/nid/me',
    'kakao': 'https://kapi.kakao.com/v2/user/me'
}

def gender_code (v):
    if not v:
        return ''
    if v.lower () in ('m', 'male'):
        return 'male'
    elif v.lower () in ('f', 'female'):
        return 'female'
    return 'etc'

def get_uid_and_profile (provider, payload):
    profile = {}
    profile ['providerId'] = provider
    if provider == 'kakao':
        id = payload ['id']
        account = payload ['kakao_account']
        profile ['displayName'] = account ['profile'].get ('nickname', '')
        profile ['photoURL'] = account ['profile'].get ('thumbnail_image_url', '')

    elif provider == 'naver':
        account = payload ['response']
        id = account ['id']
        profile ['displayName'] = account.get ('nickname', '')
        profile ['photoURL'] = account.get ('profile_image', '')

    profile ['uid'] = id
    profile ['birthday'] = account.get ('birthday', '').replace ('-', '')
    profile ['email'] = account.get ('email', '')
    profile ['gender'] = gender_code (account.get ('gender', ''))
    profile ['name'] = account.get ('name', '')
    profile ['phoneNumber'] = ''

    # make 28 bytes UID
    uid = '{}-{}'.format (provider, id)
    uid = base64.encodestring (hashlib.md5 (uid.encode ()).digest () + b'-cust') [:-1].decode ().replace ('/', '-').replace ('+', '.')
    return uid, profile
