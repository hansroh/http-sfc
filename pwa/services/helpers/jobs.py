from rs4 import psutil as pmaster
import sys

class VideoClipping (pmaster.Puppet):
  def __init__ (self, userid, websocket, executive, id, clips = 10):
    self.userid = userid
    self.websocket = websocket
    self.executive = executive
    self.id = id
    self.clips = clips
    pmaster.Puppet.__init__ (self)

  def log (self, line, *args):
    if not self.websocket:
        return
    self.websocket.send (line)

  def start (self):
    cmd = [sys.executable, self.executive, "--id", self.id, "--clips", self.clips]
    pmaster.Puppet.start (self, cmd)

# module methods -------------------------------------------------------

USERS = {}

def kill (user):
    if not has_job (user):
        return
    job = USERS.pop (user)
    job.kill ()
    return job.is_active ()

def add_job (user, job):
    global USERS

    USERS [user]= job

def has_job (user):
    global USERS

    if user not in USERS:
        return False
    if USERS [user].is_active ():
        return True
    del USERS [user]
    return False

def attach (user, websocket):
    global USERS

    if has_job (user):
         USERS [user].websocket = websocket
         USERS [user].log ("websocket reconected: {}".format (USERS [user].id))

def dettach (user):
    global USERS

    if has_job (user):
         USERS [user].websocket = None
