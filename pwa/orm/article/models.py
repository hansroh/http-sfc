from django.db import models
from member.models import User

# Create your models here.

class Category(models.Model):
    idx = models.AutoField(primary_key=True)
    user_idx = models.IntegerField()
    title = models.CharField(help_text= '카테고리명', max_length=128)
    main_cat = models.IntegerField(help_text = '메인 카테고리 (관리용:0, 사용자용:1)', default = 1)
    disp_order = models.IntegerField(default = 0, help_text = '오름차순')
    status = models.IntegerField(default = 1, help_text = '0: 삭제, 1: 정상')
    sub_title = models.CharField(max_length=128, blank=True, null=True, help_text = '카테고리 설명')
    reg_date_time = models.DateTimeField(auto_now_add = True)
    code = models.CharField(help_text = '카테고리코드(영문 소문자 공백없음)', max_length=32)
    writable = models.BooleanField(default = True)

    class Meta:
        db_table = 'category'
        verbose_name = '카테고리'
        verbose_name_plural = '카테고리 목록'

    def __str__ (self):
        return f'{self.title}'


class Content(models.Model):
    idx = models.AutoField(primary_key=True)
    user_idx = models.ForeignKey(User, models.DO_NOTHING, db_column='user_idx')
    category_idx = models.ForeignKey(Category, models.DO_NOTHING, db_column='category_idx')
    subject = models.CharField(max_length=128, blank=True, null=True)
    main_text = models.TextField(blank=True, null=True)
    reg_date_time = models.DateTimeField(auto_now_add = True)
    news_url = models.CharField(max_length=1024, blank=True, null=True)
    status = models.IntegerField(default = 1)
    site_thumbnail_uid = models.CharField(max_length=32, blank=True, null=True)
    nick_name = models.CharField(max_length=40, blank=True, null=True)
    sympathy = models.IntegerField(default = 0)
    not_sympathy = models.IntegerField(default = 0)
    comment_count = models.IntegerField(default = 0)
    reward_point = models.IntegerField(default = 0)
    report_count = models.IntegerField(default = 0)
    disp_start = models.DateTimeField(blank=True, null=True)
    disp_end = models.DateTimeField(blank=True, null=True)
    summary = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        db_table = 'contents'
        verbose_name = '게시글'
        verbose_name_plural = '게시글 목록'

    def __str__ (self):
        return f'{self.user_idx}/{self.subject [:12]}...'


class Comment(models.Model):
    idx = models.AutoField(primary_key=True)
    content_idx = models.ForeignKey(Content, models.DO_NOTHING, db_column='content_idx', blank=True, null=True)
    user_idx = models.ForeignKey(User, models.DO_NOTHING, db_column='user_idx', blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    status = models.IntegerField(default = 1)
    reg_date_time = models.DateTimeField(auto_now_add = True)
    sympathy = models.IntegerField(default = 0)
    news_url = models.CharField(max_length=1024, blank=True, null=True)
    site_thumbnail_uid = models.CharField(max_length=32, blank=True, null=True)
    not_sympathy = models.IntegerField(default = 0)
    nick_name = models.CharField(max_length=40, blank=True, null=True)
    reward_point = models.IntegerField(default = 0)
    report_count = models.IntegerField(default = 0)

    class Meta:
        db_table = 'comments'
        verbose_name = '댓글'
        verbose_name_plural = '댓글 목록'

    def __str__ (self):
        return f'{self.user_idx}'


class SiteThumbnail(models.Model):
    title = models.CharField(max_length=256, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    image = models.CharField(max_length=256, blank=True, null=True)
    idx = models.AutoField(primary_key=True)
    user_idx = models.IntegerField(blank=True, null=True)
    url = models.CharField(max_length=256, blank=True, null=True)
    site_name = models.CharField(max_length=256, blank=True, null=True)
    locale = models.CharField(max_length=10, blank=True, null=True)
    image_width = models.IntegerField(blank=True, null=True)
    image_height = models.IntegerField(blank=True, null=True)
    uid = models.CharField(unique=True, max_length=32, blank=True, null=True)
    canonical_uid = models.CharField(max_length=32, blank=True, null=True)
    type = models.CharField(max_length=32, blank=True, null=True)
    reg_time = models.DateTimeField(auto_now_add = True)

    class Meta:
        db_table = 'site_thumbnail'
        verbose_name = '기사스크랩'
        verbose_name_plural = '기사스크랩 목록'

    def __str__ (self):
        return f'{self.title}'

    def thumbnail (self):
        if self.image:
            return mark_safe ('<img src="{}" width="60">'.format (self.image))
        return ""


class UserAction(models.Model):
    idx = models.AutoField(primary_key=True)
    user_idx = models.ForeignKey(User, models.DO_NOTHING, db_column='user_idx', blank=True, null=True)
    content_idx = models.ForeignKey(Content, models.DO_NOTHING, db_column='content_idx')
    comment_idx = models.ForeignKey(Comment, models.DO_NOTHING, db_column='comment_idx', blank=True, null=True)
    action = models.IntegerField(help_text = '1: 공감, 2: 비공감, 9: 신고, 10: 신고철회')
    reg_date_time = models.DateTimeField(auto_now_add = True)
    ipaddr = models.CharField(max_length=32, blank=True, null=True)
    issued_reward_point = models.IntegerField(default = 0)

    class Meta:
        db_table = 'user_actions'
        verbose_name = '사용자액션'
        verbose_name_plural = '사용자액션 목록'

