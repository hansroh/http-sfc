from django.contrib import admin
from django.contrib import admin
from rs4.djadmin import set_title, ModelAdmin; set_title ("공감과 비판")
from django.db.models import Q, F
from django.contrib.admin import DateFieldListFilter
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from admin_numeric_filter.admin import NumericFilterModelAdmin, SingleNumericFilter, RangeNumericFilter, SliderNumericFilter

from .models import SiteThumbnail, Content, Comment
from .models import Category, UserAction

class CommentInline(admin.StackedInline):
    model = Comment
    extra = 0

@admin.register(Content)
class ContentAdmin(ModelAdmin):
    list_filter = ['status', 'category_idx']
    search_fields = ['nick_name', 'subject']
    list_display = ['subject', 'nick_name', 'status', 'reg_date_time', 'comment_count', 'sympathy', 'not_sympathy', 'reward_point', 'report_count']
    inlines = [CommentInline]
    ordering = ['-reg_date_time']
    readonly_fields = ['comment_count', 'sympathy', 'not_sympathy', 'reward_point', 'report_count']

@admin.register(Comment)
class CommentAdmin(ModelAdmin):
    list_filter = ['status']
    search_fields = ['nick_name', 'text']
    list_display = ['content_idx', 'nick_name', 'status', 'reg_date_time', 'sympathy', 'not_sympathy', 'reward_point', 'report_count']
    readonly_fields = ['sympathy', 'not_sympathy', 'reward_point', 'report_count']
    enable_add = False

@admin.register(SiteThumbnail)
class SiteThumbnailAdmin(ModelAdmin):
    list_display = ['thumbnail', 'title', 'site_name', 'reg_time']
    search_fields = ['title', 'site_name']
    enable_add = False

@admin.register(Category)
class CategoryAdmin(ModelAdmin):
    list_display = ['title', 'sub_title', 'code', 'main_cat', 'status', 'writable']
    list_filter = ['status', 'writable', 'main_cat']

@admin.register(UserAction)
class UserActionAdmin(ModelAdmin):
    list_display = ['user_idx', 'content_idx', 'comment_idx', 'action', 'ipaddr', 'issued_reward_point', 'reg_date_time']
    search_fields = ['user_idx__nick_name']
    enable_add = False
