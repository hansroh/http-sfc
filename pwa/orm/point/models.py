from django.db import models
from member.models import User

# Create your models here.

class PointTransferOrder(models.Model):
    user_idx = models.ForeignKey(User, models.DO_NOTHING, db_column='user_idx')
    amount = models.IntegerField()
    status = models.CharField(max_length=20, default = 'pending')
    idx = models.BigAutoField(primary_key=True)
    block_number = models.CharField(max_length=32, blank=True, null=True)
    tx_id = models.CharField(max_length=32, blank=True, null=True)
    reg_date = models.DateTimeField(auto_now_add = True)
    wallet_idx = models.IntegerField()
    last_updated = models.DateTimeField(auto_now = True)
    push_token = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        db_table = 'point_transfer_order'
        verbose_name = '리워드전환 신청'
        verbose_name_plural = '리워드전환 신청 목록'

    def __str__ (self):
        return f'{self.amount}p / {self.status}'


class PointTransferLog(models.Model):
    idx = models.AutoField(primary_key=True)
    reg_date_time = models.DateTimeField(auto_now_add = True)
    order_idx = models.ForeignKey(PointTransferOrder, models.DO_NOTHING, db_column='order_idx')
    status = models.CharField(max_length=20)

    class Meta:
        db_table = 'point_transfer_log'
        verbose_name = '리워드전환 처리로그'
        verbose_name_plural = '리워드전환 처리로그 목록'

    def __str__ (self):
        return f'{self.order_idx}'


