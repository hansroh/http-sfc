from django.contrib import admin

from django.contrib import admin
from django.contrib import admin
from rs4.djadmin import set_title, ModelAdmin; set_title ("공감과 비판")
from django.db.models import Q, F
from django.contrib.admin import DateFieldListFilter
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from admin_numeric_filter.admin import NumericFilterModelAdmin, SingleNumericFilter, RangeNumericFilter, SliderNumericFilter

from .models import PointTransferLog, PointTransferOrder

@admin.register(PointTransferLog)
class PointTransferLogAdmin(ModelAdmin):
    list_display = ['order_idx', 'reg_date_time', 'status']
    list_filter = ['status']
    enable_add = False

class PointTransferLogInline(admin.TabularInline):
    model = PointTransferLog
    extra = 0

@admin.register(PointTransferOrder)
class PointTransferOrderAdmin(ModelAdmin):
    list_display = ['user_idx', 'amount', 'status', 'block_number', 'tx_id', 'reg_date', 'last_updated']
    search_fields = ['user_idx__nick_name']
    list_filter = ['status']
    inlines = [PointTransferLogInline]
    enable_add = False
