# Generated by Django 2.2.11 on 2020-05-22 02:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('point', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL ("ALTER TABLE point_transfer_log ALTER COLUMN reg_date_time SET DEFAULT now();"),

        migrations.RunSQL ("ALTER TABLE point_transfer_order ALTER COLUMN status SET DEFAULT 'penging';"),
        migrations.RunSQL ("ALTER TABLE point_transfer_order ALTER COLUMN reg_date SET DEFAULT now();"),
        migrations.RunSQL ("ALTER TABLE point_transfer_order ALTER COLUMN last_updated SET DEFAULT now();"),
    ]
