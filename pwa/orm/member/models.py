from django.db import models

# Create your models here.


class UserLog(models.Model):
    idx = models.AutoField(primary_key=True)
    user_idx = models.ForeignKey('User', models.DO_NOTHING, db_column='user_idx')
    reg_date_time = models.DateTimeField(auto_now_add = True)
    action_req = models.TextField(blank=True, null=True)
    log_type = models.CharField(max_length=128)

    class Meta:
        db_table = 'user_log'
        verbose_name = '사용자로그'
        verbose_name_plural = '사용자로그 목록'


class User(models.Model):
    idx = models.AutoField(primary_key=True)
    uid = models.CharField(unique=True, max_length=45)
    provider = models.CharField(max_length=45, blank=True, null=True)
    nick_name = models.CharField(max_length=45, blank=True, null=True)
    name = models.CharField(max_length=45, blank=True, null=True)
    phone_no = models.CharField(max_length=20, blank=True, null=True)
    open_info = models.CharField(max_length=45, blank=True, null=True)
    email = models.CharField(max_length=128, blank=True, null=True)
    registration_no = models.CharField(max_length=6, blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    reg_date_time = models.DateTimeField(auto_now_add = True)
    photo_url = models.CharField(max_length=256, blank=True, null=True)
    status = models.CharField(max_length=128, blank=True, null=True)
    lev = models.CharField(max_length=20, default = 'user')
    last_updated = models.DateTimeField(auto_now = True)
    postcode = models.CharField(max_length=6, blank=True, null=True)
    address1 = models.CharField(max_length=128, blank=True, null=True)
    address2 = models.CharField(max_length=128, blank=True, null=True)
    email_verified = models.BooleanField(blank=True, null=True)

    class Meta:
        db_table = 'users'
        verbose_name = '사용자'
        verbose_name_plural = '사용자 목록'

    def __str__ (self):
        return f'{self.nick_name}' or '노닉'


class Reward(models.Model):
    idx = models.AutoField(primary_key=True)
    user_idx = models.ForeignKey('User', models.DO_NOTHING, db_column='user_idx')
    amount = models.BigIntegerField(default = 0, help_text = '현재 리워드')
    accumulated = models.BigIntegerField(default = 0, help_text = '누적 리워드')
    content_count = models.IntegerField(default = 0, help_text = '작성 글수')
    comment_count = models.IntegerField(default = 0, help_text = '작성 댓글수')
    report_count = models.IntegerField(default = 0, help_text = '신고건수')
    sympathy = models.IntegerField(default = 0, help_text = '공감 받은 수')
    not_sympathy = models.IntegerField(default = 0, help_text = '비공감 받은 수')
    reg_date_time = models.DateTimeField(auto_now_add = True)
    uptimestamp = models.DateTimeField(auto_now = True)

    class Meta:
        db_table = 'rewards'
        verbose_name = '리워드'
        verbose_name_plural = '리워드 목록'

    def __str__ (self):
        return f'{self.user_idx}'



