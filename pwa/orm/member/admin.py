from django.contrib import admin

from django.contrib import admin
from django.contrib import admin
from rs4.djadmin import set_title, ModelAdmin; set_title ("공감과 비판")
from django.db.models import Q, F
from django.contrib.admin import DateFieldListFilter
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from admin_numeric_filter.admin import NumericFilterModelAdmin, SingleNumericFilter, RangeNumericFilter, SliderNumericFilter

from .models import User, UserLog, Reward

# User --------------------------------------------
@admin.register(User)
class UserAdmin(ModelAdmin):
    list_display = ['nick_name', 'provider', 'email', 'reg_date_time', 'status', 'lev']
    list_filter = ['status', 'provider']
    list_editable = []
    search_fields = ['nick_name', 'email', 'phone_no', 'name', 'registration_no']
    readonly_fields = ["uid"]
    enable_add = False

@admin.register(Reward)
class RewardAdmin(ModelAdmin):
    list_display = ['user_idx', 'amount', 'reg_date_time', 'uptimestamp']
    search_fields = ['user_idx__nick_name']
    readonly_fields = []
    enable_add = False

@admin.register(UserLog)
class UserLogAdmin(ModelAdmin):
     list_display = ['user_idx', 'log_type', 'reg_date_time']
     search_fields = ['user_idx__nick_name']
     list_filter = ['log_type']




