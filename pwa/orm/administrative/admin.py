from django.contrib import admin
from django.contrib import admin
from rs4.djadmin import set_title, ModelAdmin; set_title ("공감과 비판")
from django.db.models import Q, F
from django.contrib.admin import DateFieldListFilter
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from admin_numeric_filter.admin import NumericFilterModelAdmin, SingleNumericFilter, RangeNumericFilter, SliderNumericFilter

from .models import Qna, Config, DaemonLog

@admin.register(Qna)
class QnaAdmin(ModelAdmin):
    list_display = ['user_idx', 'q_title', 'q_time', 'manager_idx', 'status', 'a_time', 'is_faq']
    search_fields = ['user_idx__nick_name', 'title', 'site_name']
    list_filter = ['status', 'is_faq']
    enable_add = False


@admin.register(Config)
class ConfigAdmin(ModelAdmin):
    list_display = ['item', 'val', 'reg_date_time']


@admin.register(DaemonLog)
class DaemonLogAdmin(ModelAdmin):
    list_display = ['idx', 'name', 'act', 'log_time']
    enable_add = False
