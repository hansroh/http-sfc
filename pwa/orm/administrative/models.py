# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils.safestring import mark_safe
from member.models import User

class Config(models.Model):
    idx = models.AutoField(primary_key=True)
    item = models.CharField(max_length=20, help_text = '설정이름')
    val = models.IntegerField(blank=True,null=True, help_text = '설정값')
    text = models.TextField(blank=True,null=True)
    reg_date_time = models.DateTimeField(auto_now_add = True)
    user_idx = models.IntegerField(null = True, blank = True)

    class Meta:
        db_table = 'configs'
        verbose_name = '설정'
        verbose_name_plural = '설정 목록'

    def __str__ (self):
        return f'{self.item}'


class Qna(models.Model):
    idx = models.AutoField(primary_key=True)
    user_idx = models.ForeignKey(User, models.DO_NOTHING, related_name = 'fk_qna_user_idx', db_column='user_idx')
    manager_idx = models.ForeignKey(User, models.DO_NOTHING, related_name = 'fk_qnamanager_idx', db_column='manager_idx', blank=True, null=True)
    q_title = models.CharField(max_length=128, blank=True, null=True)
    q_text = models.TextField(blank=True, null=True)
    a_title = models.CharField(max_length=128, blank=True, null=True)
    a_text = models.TextField(blank=True, null=True)
    status = models.BigIntegerField(default = 0, help_text = '0: 질문, 1: 질문확인, 2: 답변완료')
    q_time = models.DateTimeField(auto_now_add = True)
    a_time = models.DateTimeField(blank=True, null=True)
    nick_name = models.CharField(max_length=45, blank=True, null=True)
    is_faq = models.BooleanField(default = False)
    push_token = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        db_table = 'qna'
        verbose_name = '1:1 문의'
        verbose_name_plural = '1:1 문의 목록'

    def __str__ (self):
        return f'{self.user_idx}'


class DaemonLog (models.Model):
    idx = models.AutoField(primary_key=True)
    log_time = models.DateTimeField(auto_now_add = True)
    name = models.CharField(max_length=24)
    act = models.CharField(max_length=128)
    user_idx = models.ForeignKey(User, models.DO_NOTHING, null = True, db_column='user_idx')

    class Meta:
        db_table = 'daemon_log'
        verbose_name = '데몬로그'
        verbose_name_plural = '데몬로그 목록'
