from atila import Atila
import os
from services import backbone
from services.backbone import auth, users
from services.views import examples

app = Atila (__name__)

app.mount ('/apis', backbone)
app.mount ('/examples', examples)

@app.route ('/', methods = ['GET'])
def index (was):
    return was.redirect ('/examples')

@app.route ('/status')
def status (was, f = ''):
    return was.status (f)
