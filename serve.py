#! /usr/bin/env python3
# Created on Feb 12, 2020 by Hans Roh (hansroh@gmail.com)

import skitai

if __name__ == '__main__':
    with skitai.preference () as pref:
        pref.securekey = "2adabeb7-##########-2f7a07785413"
        pref.max_client_body_size = 2 << 32
        pref.access_control_allow_origin = ["http://localhost:5000"]

        if skitai.is_devel ():
            pref.expose_spec = True
            pref.config.DEFAULT_MAX_AGE = 2
            pref.config.REFRESH_TOKEN_TIMEOUT = 1800
            pref.config.ACCESS_TOKEN_TIMEOUT = 180

        pref.config.SERVICE_WORKER = True
        skitai.mount ('/', 'http-sfc/wsgi:app', pref)

    skitai.mount ("/", 'http-sfc/static')
    skitai.run (name='http-sfc', port = 5000, threads = 2)
