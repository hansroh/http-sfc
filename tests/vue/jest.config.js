module.exports = {
    verbose: true,
    moduleFileExtensions: [
      'js',
      'jsx',
      'json',
      'vue'
    ],
    transform: {
      '^.+\\.vue$': 'vue-jest',
      '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
      '^.+\\.jsx?$': 'babel-jest'
    },
    transformIgnorePatterns: [
      '/node_modules/'
    ],
    moduleNameMapper: {
      '^@/(.*)$': '<rootDir>/../../pwa/static/$1'
    },
    snapshotSerializers: [
      'jest-serializer-vue'
    ],
    testMatch: [
      '**/*.(test|spec).(js|jsx|ts|tsx)',
    ],
    testURL: 'http://localhost:5000/',
    watchPlugins: [
      'jest-watch-typeahead/filename',
      'jest-watch-typeahead/testname'
    ],
    collectCoverage: false,
    collectCoverageFrom: [
      '<rootDir>/../../pwa/static/**/*.{js|vue}',
      '!/node_modules/'
    ],
    coverageReporters: [
      'text-summary', 'html'
    ],
    setupFiles: [
      "<rootDir>/jest.init.build.js"
    ]
  }
