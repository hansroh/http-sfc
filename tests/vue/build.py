import os

INCLUDES = [
    'setup/vue-setup@0.1.js',
    'setup/element-ui-setup@0.1.js',
    'js/setup.js'
]
with open ('jest.init.js') as f:
    template = f.read ()

for fn in INCLUDES:
    pos = '// INCLUDE {}'.format (fn)
    assert template.find (pos)

    path = os.path.join (os.path.dirname (__file__), '../../pwa/static/assets', fn)
    with open (path) as f:
        data = f.read ()
        template = template.replace (pos, data)

with open ('jest.init.build.js', 'w') as f:
    f.write (template)


