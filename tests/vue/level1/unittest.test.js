import { shallowMount } from '@vue/test-utils'
import UnitTest from '@/components/unittest.vue'

describe('unittest.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(UnitTest, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })
})

