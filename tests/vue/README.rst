Installation
======================

.. code:: bash

  sudo apt install -y npm
  cd tests/vue
  npm install

  ./vuetest



참고: 모듈 수동설치

.. code:: bash

  npm install -g @vue/cli 
  npm install axios element-ui
  npm install --save-dev jest @vue/test-utils vue-jest babel-jest axios-mock-adapter

  

Ttest Example
================

.. code:: bash

  # level1/firebase-account.test.js

  import { shallowMount, mount } from '@vue/test-utils'
  import FirebaseAccount from '@/components/auth/firebase-account.vue'

  const mocks = $mocks ()
  const profileEditUrl = '/'

  describe('firebase-account.vue', () => {
    it('renders register button', () => {
      mocks.$args.return_url = '/'
      const wrapper = shallowMount (FirebaseAccount, {
        propsData: { profileEditUrl, isSignup: true },
        mocks,
      })
      expect(wrapper.vm.$refs.profileForm).toBeDefined ()
      expect(wrapper.text()).toBe('Register')
    })

    it('renders return_url should be / if return_url is app', () => {
      mocks.$proxy.onPatch ('/apis/users/me/profile').reply (201, {new_nick_name: 'asdada'})
      mocks.$args.return_url = 'app'
      mocks.$urlspecs = {"loginUrl": {"path": "/apis/auth/login_with_email", "params": [], "query": ["email"]}, "loginWithFirebaseIdToken": {"path": "/apis/auth/login_with_firebase_id_token", "params": [], "query": ["id_token", "profile", "returns", "decoded_token"]}, "firebaseCustomToken": {"path": "/apis/auth/firebase_custom_token", "params": [], "query": ["provider", "access_token", "payload"]}, "firebaseUsers": {"path": "/apis/auth/firebase/users/:uid", "params": ["uid"], "query": []}, "loginForm": {"path": "/apis/auth/login", "params": [], "query": ["provider", "return_url", "state"]}, "signupForm": {"path": "/apis/auth/signup", "params": [], "query": ["return_url"]}, "nickName": {"path": "/apis/auth/nick_names/:nick_name", "params": ["nick_name"], "query": []}, "updateProfile": {"path": "/apis/users/:uid/profile", "params": ["uid"], "query": []}}
      const wrapper = shallowMount (FirebaseAccount, {
        propsData: { profileEditUrl },
        mocks,
      })
      expect(wrapper.vm.$args.return_url).toBe('/')
      expect(wrapper.vm.$ss.get ('returnUrl')).toBe('app')

      wrapper.vm.postProfile ()
    })
  })


Mocking
=============

Parameter Mocking
-------------------------

.. code:: bash

  const mocks = $mocks ()

  mocks.$args.srch_type='nick_name'
  mocks.$args.srch_txt='세미콘'
  mocks.$route = {path: '/some/path'}
  mocks.$urlspecs = {"loginUrl": {"path": "/apis/auth/login_with_email", "params": [], "query": ["email"]}


Data Mocking
------------------------

.. code:: bash

  const mocks = $mocks ()

  mocks.context = {
    items: [{idx: 1, subkect: '해피홈'}, {idx: 4, subkect: '퍼니홈'}],
    record_count: 65
  }


API Mocking
----------------

.. code:: bash

  const mocks = $mocks ()

  mocks.$proxy.onPatch ('/apis/users/me/profile').reply (201, {new_nick_name: 'asdada'})

