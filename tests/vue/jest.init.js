import Element from 'element-ui'
import axios  from 'axios'
import MockAdapter from 'axios-mock-adapter'
import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

const vuexItems = {}
const protoMethods = {}
const interactionMethods = {}
const vueUtilMethods = {}

// include setup files ---------------------------------

// INCLUDE setup/vue-setup@0.1.js
// INCLUDE setup/element-ui-setup@0.1.js
// INCLUDE js/setup.js

// make globals ----------------------------------------

Vue.use (Element)
global.httpVueLoader = require ('@/libs/http-vue-loader@1.4.2.js')
global.axios = axios
global.mapVuexItems = function () {}
global.prefetch = (href)  => {}
global.websocketMethods = function () {
  return websocketMethods_
}
axios.defaults.withCredentials = true
axios.defaults.baseURL = 'http://localhost:9014'

const mocks = {...vuexItems}
mocks.$cloak = false
mocks.$txnid = 'TXN_ID'
mocks.$ut = vueUtilMethods
mocks.$http = axios
mocks.$proxy = new MockAdapter (axios)

Object.assign (protoMethods, interactionMethods)
for (let [k, v] of Object.entries (protoMethods)) {
  mocks [k] = v
}

global.$mocks = function(debug = false) {
  const mocks_ = Object.assign({}, mocks)
  mocks_.$debug = debug
  mocks_.$store = {}
  mocks_.$args = {}
  mocks_.$urlspecs = {}
  mocks_.$route = {}
  mocks_.$router = new VueRouter ()
  mocks_.$bus = new Vue()
  mocks_.$store.state = {}
  mocks_.$proxy.reset ()
  return mocks_
}
