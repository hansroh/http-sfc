import pytest
from pprint import pprint
from generate_test_data import IDX, UID, SITE_URL
from copy import copy
import time

def get_article (stub):
    global ARTICLE
    resp = stub.apis.articles.get (srch_type = 'nick_name', srch_txt = '세미콘 네트웍스', sort = 'idx', limit = 100)
    assert resp.status_code == 200
    assert resp.data ['record_count']
    assert resp.data ['item_list']
    for item in resp.data ['item_list']:
        if item ['sympathy'] > 0 and item ['not_sympathy'] > 0 and item ['comment_count'] > 0:
            first = item
            break

    resp = stub.articles (first ['idx']).get ()
    return resp.data


def test_actions_anon (stub, csrf):
    article = get_article (stub)
    content_idx = article ['item']['idx']
    resp = stub.apis.articles (content_idx).actions.post ({'action': 'sympathy'})
    assert resp.status_code == 400
    resp = stub.apis.articles (content_idx).actions.post ({'action': 'sss'}, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 400

    for action in ('sympathy', 'not_sympathy', 'report'):
        resp = stub.apis.articles (content_idx).actions.post ({'action': action}, headers = {'X-XSRF-Token': csrf})
        assert resp.status_code in (201, 406)
        resp = stub.apis.articles (content_idx).actions.post ({'action': action}, headers = {'X-XSRF-Token': csrf})
        assert resp.status_code == 406


def test_actions (stub, exister, csrf):
    article = get_article (stub)
    content_idx = article ['item']['idx']

    resp = stub.apis.articles (content_idx).actions.get ()
    assert resp.status_code == 401
    resp = exister (UID, 'user', auth = True)
    resp = stub.apis.articles (content_idx).actions.get ()
    assert resp.status_code == 200

    resp = stub.apis.articles (content_idx).actions.post ({'action': 'sympathy'})
    assert resp.status_code == 400

    report_count = article ['item']['report_count']
    sympathy = article ['item']['sympathy']
    not_sympathy = article ['item']['not_sympathy']

    resp = stub.apis.articles (content_idx).actions.post ({'action': 'report'}, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201
    assert resp.data ['report_count'] in (report_count - 1, report_count + 1)

    resp = stub.apis.articles (content_idx).actions.post ({'action': 'report'}, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201
    assert resp.data ['report_count'] == report_count

    resp = stub.apis.articles (content_idx).actions.post ({'action': 'sympathy'}, headers = {'X-XSRF-Token': csrf})
    if resp.status_code == 406:
        resp = stub.apis.articles (content_idx).actions.post ({'action': 'not_sympathy'}, headers = {'X-XSRF-Token': csrf})
        assert resp.data ['sympathy'] == sympathy - 1
        sympathy -= 1
        assert resp.data ['not_sympathy'] == not_sympathy + 1
        not_sympathy += 1
    else:
        assert resp.data ['sympathy'] == sympathy + 1
        sympathy += 1

    report_count = resp.data ['report_count']
    sympathy = resp.data ['sympathy']
    not_sympathy = resp.data ['not_sympathy']

    resp = stub.apis.articles (content_idx).actions.post ({'action': 'not_sympathy'}, headers = {'X-XSRF-Token': csrf})
    if resp.status_code == 406:
        resp = stub.apis.articles (content_idx).actions.post ({'action': 'sympathy'}, headers = {'X-XSRF-Token': csrf})
        assert resp.data ['sympathy'] == sympathy + 1
        assert resp.data ['not_sympathy'] == not_sympathy - 1
    else:
        assert resp.data ['not_sympathy'] == not_sympathy + 1


def test_actions_on_comment (stub, exister, csrf):
    article = get_article (stub)
    content_idx = article ['item']['idx']

    resp = exister (UID, 'user', auth = True)
    resp = stub.apis.articles (content_idx).comments.get ()
    assert resp.status_code == 200

    comment = resp.data ['comments'][0]
    comment_idx = comment ['idx']

    resp = stub.apis.articles (content_idx).comments (comment_idx).actions.post ({'action': 'sympathy'})
    assert resp.status_code == 400

    report_count = comment ['report_count']
    sympathy = comment ['sympathy']
    not_sympathy = comment ['not_sympathy']

    resp = stub.apis.articles (content_idx).comments (comment_idx).actions.post ({'action': 'report'}, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201
    assert resp.data ['report_count'] in (report_count - 1, report_count + 1)

    resp = stub.apis.articles (content_idx).comments (comment_idx).actions.post ({'action': 'report'}, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201
    assert resp.data ['report_count'] == report_count

    resp = stub.apis.articles (content_idx).comments (comment_idx).actions.post ({'action': 'sympathy'}, headers = {'X-XSRF-Token': csrf})
    if resp.status_code == 406:
        resp = stub.apis.articles (content_idx).comments (comment_idx).actions.post ({'action': 'not_sympathy'}, headers = {'X-XSRF-Token': csrf})
        assert resp.data ['sympathy'] == sympathy - 1
        sympathy -= 1
        assert resp.data ['not_sympathy'] == not_sympathy + 1
        not_sympathy += 1
    else:
        assert resp.data ['sympathy'] == sympathy + 1
        sympathy += 1

    report_count = resp.data ['report_count']
    sympathy = resp.data ['sympathy']
    not_sympathy = resp.data ['not_sympathy']
    resp = stub.apis.articles (content_idx).comments (comment_idx).actions.post ({'action': 'not_sympathy'}, headers = {'X-XSRF-Token': csrf})
    if resp.status_code == 406:
        resp = stub.apis.articles (content_idx).comments (comment_idx).actions.post ({'action': 'sympathy'}, headers = {'X-XSRF-Token': csrf})
        assert resp.data ['sympathy'] == sympathy + 1
        assert resp.data ['not_sympathy'] == not_sympathy - 1
    else:
        assert resp.data ['not_sympathy'] == not_sympathy + 1


