import pytest
from pprint import pprint
from generate_test_data import IDX, UID, IPSUM
from datetime import datetime, timedelta

payload = dict (
    q_title = '질문입니다',
    q_text = '증인은 답변해 주세요'
)


payload2 = dict (
    a_title = '답변입니다',
    a_text = '만족하셨나요?'
)

def test_qnas (stub, exister, csrf):
    resp = stub.apis.users ('me').qnas.get ()
    assert resp.status_code == 401

    resp = stub.apis.users ('notme').qnas.get ()
    assert resp.status_code == 401

    resp = exister (UID, 'user', auth = True)

    resp = stub.apis.users ('me').qnas.get ()
    assert resp.status_code == 200
    assert 'qnas' in resp.data
    assert 'qna_count' in resp.data
    resp = stub.apis.users ('me').qnas.get (status = 'opened', limit = 10)
    assert resp.status_code == 200
    assert 'qnas' in resp.data
    assert 'qna_count' in resp.data

    resp = stub.apis.users ('me').qnas.post (payload)
    assert resp.status_code == 201
    assert 'url' in resp.data
    idx = resp.data ['url'].split ('/')[-1]

    resp = stub.apis.users (100).qnas (idx).patch (payload)
    assert resp.status_code == 403

    fake = payload.copy ()
    fake ['q_title'] = '<p>asdasd</p>'
    resp = stub.apis.users ('me').qnas (idx).patch (fake)
    assert resp.status_code == 200

    fake ['q_text'] = '<script>asdasd</script>'
    resp = stub.apis.users ('me').qnas (idx).patch (fake)
    assert resp.status_code == 400

    resp = stub.apis.users ('me').qnas (idx).patch (payload)
    assert resp.status_code == 200

    resp = stub.apis.users ('me').qnas (idx).delete ()
    assert resp.status_code == 201

    resp = stub.apis.qnas (idx).answer ().put (payload2)
    assert resp.status_code == 403


def test_qnas_answer (stub, exister, csrf):
    resp = exister (UID, 'user', auth = True)
    resp = stub.apis.users ('me').qnas.post (payload)
    assert resp.status_code == 201
    assert 'url' in resp.data
    idx = resp.data ['url'].split ('/')[-1]

    resp = exister (UID, 'staff', auth = True)

    resp = stub.apis.users ('notme').qnas.get (status = 'waiting', srch_type = 'q_title', srch_txt = '짊문')
    assert resp.status_code == 200

    resp = stub.apis.users ('notme').qnas.get (status = 'waiting', srch_type = 'uid', srch_txt = 'UNITTEST')
    assert resp.status_code == 200

    resp = stub.apis.users ('notme').qnas.get (status = 'waiting', srch_type = 'nick_name', srch_txt = '짊문')
    assert resp.status_code == 200

    resp = stub.apis.users ('notme').qnas.get (status = 'waiting')
    assert resp.status_code == 200

    resp = stub.apis.users (UID).qnas (idx).get ()
    assert resp.status_code == 200

    resp = stub.apis.qnas (idx).ticket.put ({'confirm': True})
    assert resp.status_code == 201

    resp = stub.apis.users (UID).qnas (idx).get ()
    assert resp.data ['qna']['status'] == 1
    assert not resp.data ['qna']['a_title']

    resp = stub.apis.qnas (idx).answer.put (payload2)
    assert resp.status_code == 201

    resp = stub.apis.users (UID).qnas (idx).get ()
    assert resp.data ['qna']['status'] == 2
    assert resp.data ['qna']['a_title']
    assert resp.data ['qna']['a_time']

    payload2 ['a_text'] = '<a href="javascript: location.href=/">'
    resp = stub.apis.qnas (idx).answer.put (payload2)
    assert resp.status_code == 400

    resp = stub.apis.users (UID).qnas (idx).delete ()
    assert resp.status_code == 403

    resp = stub.apis.qnas (idx).delete ()
    assert resp.status_code == 201
