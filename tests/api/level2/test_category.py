import pytest
from pprint import pprint
from generate_test_data import IDX, UID, SITE_URL

payload = dict (
    title = 'Test',
    main_cat = 0,
    sub_title = 'Test',
    writable = True,
)

def test_category (stub, exister, csrf):
    resp = stub.apis.articles.categories.get ()
    assert resp.data ['categories'][0]['idx']

    current_cat = None
    for cat in resp.data ['categories']:
        current_cat = cat
    code = current_cat ['code']

    resp = stub.apis.articles.categories (code).put (payload)
    assert resp.status_code == 401

    resp = stub.apis.articles.categories (code).patch (payload)
    assert resp.status_code == 401

    resp = stub.apis.articles.categories (code).delete ()
    assert resp.status_code == 401

    resp = exister (UID, 'user', auth = True)
    resp = stub.apis.articles.categories (code).put (payload)
    assert resp.status_code == 403

    resp = exister (UID, 'staff', auth = True)
    resp = stub.apis.articles.categories (code).put (payload)
    assert resp.status_code == 400


def test_category (stub, exister, csrf):
    resp = stub.apis.articles.categories.get ()
    assert resp.data ['categories'][0]['idx']

    current_cat = None
    for cat in resp.data ['categories']:
        current_cat = cat
    code = current_cat ['code']

    resp = exister (UID, 'staff', auth = True)
    resp = stub.apis.articles.categories (code).put (payload, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 409

    resp = stub.apis.articles.categories ('test').delete (real = 'true', headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201

    for i in range (2):
        resp = stub.apis.articles.categories ('test').put (payload, headers = {'X-XSRF-Token': csrf})
        if resp.status_code == 409:
            resp = stub.apis.articles.categories ('test').delete (hard = 'true', headers = {'X-XSRF-Token': csrf})
            assert resp.status_code == 201
        else:
            break

    assert resp.status_code == 201
    assert 'url' in resp.data

    found = []
    resp = stub.apis.articles.categories.get ()
    for each in resp.data ['categories']:
        print (each)
        found.append (int (each ['code'] == 'test'))
    assert sum (found) == 1

    resp = stub.apis.articles.categories ('test').patch (payload, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 200
    assert 'url' in resp.data

    resp = stub.apis.articles.categories ('test').delete (headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201

    resp = stub.apis.articles.categories ('test').delete (hard = 'true', headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201






