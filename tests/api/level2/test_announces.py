import pytest
from pprint import pprint
from generate_test_data import IDX, UID, IPSUM
from datetime import datetime, timedelta

payload = dict (
    category_idx = 1,
    subject = '[공지] 공지사항입니다',
    main_text = '<p>{}</p>'.format (IPSUM),
    disp_start = datetime.now ().timestamp (),
    disp_end = (datetime.now () + timedelta (days = 3)).timestamp ()
)


def test_announces_insert (stub, exister, csrf):
    resp = stub.apis.announces.get ()
    assert resp.status_code == 200
    if resp.data ['record_count'] == 0:
        resp = exister (UID, 'staff', auth = True)
        resp = stub.apis.announces.post (payload, headers = {'X-XSRF-Token': csrf})
        assert resp.status_code == 201


def test_announces (stub, exister, csrf):
    resp = stub.apis.announces.get ()
    assert resp.status_code == 200
    assert resp.data ['item_list']
    assert resp.data ['record_count']
    assert 'reward_point' not in resp.data ['item_list'][0]

    resp = stub.apis.announces.get (category = 2)
    assert resp.status_code == 400

    resp = stub.apis.announces.get (srch_type = 'nick_name', srch_txt = '관리자')
    assert resp.status_code == 200
    assert resp.data ['item_list']
    resp = stub.apis.announces (resp.data ['item_list'][0]['idx']).get ()
    assert resp.status_code == 200
    assert 'item' in resp.data
    assert 'main_text' in resp.data

    resp = stub.apis.announces.post (payload)
    assert resp.status_code == 401

    resp = stub.apis.announces.post (payload, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 401

    resp = exister (UID, 'user', auth = True)

    resp = stub.apis.announces.post (payload, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 403

def test_announces_ops (stub, exister, csrf):
    resp = exister (UID, 'staff', auth = True)

    temp = payload.copy ()
    temp.pop ('disp_start')
    resp = stub.apis.announces.post (temp, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 400

    resp = stub.apis.announces.post (payload, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201
    ann_idx = resp.data ['url'].split ("/")[-1]

    temp = payload.copy ()
    temp.pop ('disp_start'); temp.pop ('disp_end')

    resp = stub.apis.announces (ann_idx).patch (temp)
    assert resp.status_code == 400

    resp = stub.apis.announces (ann_idx).patch (temp, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201

    resp = stub.apis.announces (ann_idx).patch (payload, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201

    resp = stub.apis.announces (ann_idx).delete ()
    assert resp.status_code == 400

    resp = stub.apis.announces (ann_idx).delete (headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 204
