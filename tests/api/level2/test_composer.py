import pytest
from pprint import pprint
import os

UID = 'UNITTEST'

def test_article_media_upload (engine, stub, loginer):
    resp = loginer (UID, 'user', auth = True)
    # TODO

def test_articles_scraper (stub, loginer):
    resp = loginer (UID, 'user', auth = True)
    resp = stub.apis.composer.scraper.get (url = 'sample')
    assert resp.status_code == 200
    assert resp.data ['item']['canonical_uid']
    assert resp.data ['callback']
    uid = resp.data ['callback'].split ('/')[-1]
    resp = stub.apis.composer.scraper (uid).put (resp.data ['item'])

    resp = stub.apis.composer.scraper.post ({"url": "https://www.yna.co.kr/view/AKR20200219076900004"})
    assert resp.status_code == 200
    assert resp.data ['cached'] is True

    resp = stub.apis.composer.scraper.post ({"url": "https://www.yna.co.kr/view/AKR20200219076900004?category=social"})
    assert resp.status_code == 200

    resp = stub.apis.composer.scraper (uid).delete ()
    assert resp.status_code == 204

    if os.environ.get ("CI_COMMIT_TITLE"):
        return

    if not os.environ.get ("CI_COMMIT_TITLE"):
        TESTS = (
            "https://www.yna.co.kr/view/AKR20200219076900004",
            "https://www.youtube.com/watch?v=fPk24v4MTAA",
            "https://youtu.be/fPk24v4MTAA",
            "https://news.naver.com/main/read.nhn?mode=LSD&mid=shm&sid1=100&oid=022&aid=0003454902",
            "https://news.v.daum.net/v/20200409110055298",
            "https://www.facebook.com/ks.jung.3?__tn__=%2CdC-R-R&eid=ARB1C-EeFfXZdYI8EXQ9CNVLH-R49UmcNiOMXWG_DD23pgySJl7Ur6u8cqBBl4pVbkUze9SJ_jKI3UCs&hc_ref=ARQjFqNruwLBIp_HAx5I_4SZbLd-lQgiOlVjQvqgMcmSEn7_ISLnDZjcJfQ51BAdTB4&fref=nf",
            "https://edition.cnn.com/2020/04/08/business/china-us-coronavirus-workers-intl-hnk/index.html"
        )

    for each in TESTS:
        resp = stub.apis.composer.scraper.post ({"url": each, 'force': True})
        print (each)
        assert resp.status_code == 200
        assert resp.data ['item']['image']