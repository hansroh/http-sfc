import pytest
from pprint import pprint
from generate_test_data import IDX, UID, SITE_URL
from copy import copy
import time

def get_article (stub):
    global ARTICLE
    resp = stub.apis.articles.get (srch_type = 'nick_name', srch_txt = '세미콘 네트웍스', sort = 'idx')
    assert resp.status_code == 200
    assert resp.data ['record_count']
    assert resp.data ['item_list']
    first = resp.data ['item_list'][0]
    resp = stub.articles (first ['idx']).get ()
    return resp.data

def test_comments (stub, exister, csrf):
    article = get_article (stub)
    content_idx = article ['item']['idx']
    resp = stub.apis.articles (content_idx).comments.get (limit = 3)
    assert resp.data ['comments']
    payload = {
        'text': '새댓글',
        'limit': 3,
        'news_url': 'http://www.sss,com'
    }
    resp = stub.apis.articles (content_idx).comments.post (payload)
    assert resp.status_code == 401

    resp = exister (UID, 'user', auth = True)
    resp = stub.apis.articles (content_idx).comments.post (payload)
    assert resp.status_code == 400

    resp = stub.apis.articles (content_idx).comments.post (payload, headers = {'X-XSRF-TOKEN': csrf})
    assert resp.status_code == 200
    comment_count = resp.data ['comment_count']

    resp = stub.apis.articles (content_idx).comments.post (payload, headers = {'X-XSRF-TOKEN': csrf})
    assert resp.status_code == 200
    assert resp.data ['comment_count'] == comment_count + 1

    resp = stub.apis.articles (content_idx).comments.post (payload, headers = {'X-XSRF-TOKEN': csrf})
    assert resp.status_code == 200
    assert resp.data ['comment_count'] == comment_count + 2
    comment_count = resp.data ['comment_count']
    comment_idx = resp.data ['comments'][0]['idx']

    payload = {
        'text': '새댓글'
    }
    resp = stub.apis.articles (content_idx).comments (0).patch (payload, headers = {'X-XSRF-TOKEN': csrf})
    assert resp.status_code == 410

    resp = stub.apis.articles (content_idx).comments (0).patch (payload)
    assert resp.status_code == 400

    resp = stub.apis.articles (content_idx).comments (comment_idx).patch (payload, headers = {'X-XSRF-TOKEN': csrf})
    assert resp.status_code == 200

    resp = stub.apis.articles (content_idx).comments.get (limit = 3)
    assert resp.data ['comment_count'] == comment_count

    resp = stub.apis.articles (content_idx).comments (comment_idx).delete (headers = {'X-XSRF-TOKEN': csrf})
    assert resp.status_code == 201

    resp = stub.apis.articles (content_idx).comments.get (limit = 3)
    assert resp.data ['comment_count'] == comment_count

    resp = stub.apis.users ('me').comments.get (limit = 3)
    assert resp.status_code == 200
    assert 'comments' in resp.data
    assert 'comment_count' in resp.data
