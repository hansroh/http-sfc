import pytest
from pprint import pprint
from generate_test_data import IDX, UID

def test_articles (stub):
    resp = stub.apis.articles.get ()
    assert resp.status_code == 200
    assert resp.data ['item_list']
    assert resp.data ['record_count']
    assert 'reward_point' not in resp.data ['item_list'][0]

    resp = stub.apis.articles.get (category = 2)
    assert resp.status_code == 200
    assert resp.data ['item_list']
    assert resp.data ['record_count']

    resp = stub.apis.articles.get (category = 2, srch_type = 'subject', srch_txt = '나')
    assert resp.status_code == 200
    assert resp.data ['item_list']

    resp = stub.apis.articles.get (category = 2, srch_type = 'nick_name', srch_txt = '세미콘')
    assert resp.status_code == 200
    assert resp.data ['item_list']

    resp = stub.apis.articles.get (category = 2, srch_type = 'nick_name_exact', srch_txt = '세미콘')
    assert resp.status_code == 200
    assert not resp.data ['item_list']


def test_articles_private (stub, exister):
    resp = exister (UID, 'user', auth = True)
    resp = stub.apis.users (UID).articles.get ()
    assert resp.status_code == 403

    resp = stub.apis.users ('me').articles.get ()
    assert resp.status_code == 200
    assert resp.data ['item_list']
    assert resp.data ['record_count']
    assert 'reward_point' in resp.data ['item_list'][0]
