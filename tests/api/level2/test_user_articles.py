import pytest
from pprint import pprint
from generate_test_data import IDX, UID

def test_articles_private (stub, exister):
    resp = exister (UID, 'user', auth = True)
    resp = stub.apis.users (UID).articles.get ()
    assert resp.status_code == 403

    resp = stub.apis.users ('me').articles.get ()
    assert resp.status_code == 200
    assert resp.data ['item_list']
    assert resp.data ['record_count']
    assert 'reward_point' in resp.data ['item_list'][0]

def test_user_search (stub, exister):
    resp = exister (UID, 'staff', auth = True)
    resp = stub.apis.users.get ()
    assert resp.status_code == 200
    resp = stub.apis.users.get (limit = 2)
    assert resp.status_code == 200
    assert len (resp.data ['users']) == 2
    resp = stub.apis.users.get (srch_type = 'nick_name', srch_txt = '세미콘')
    assert resp.status_code == 200
    resp = stub.apis.users.get (srch_type = 'nick_name', srch_txt = '세미콘', sort = "reg_date_time")
    assert resp.status_code == 200

    resp = stub.apis.users.get (srch_type = 'nick_name', srch_txt = '세미콘', sort = "report_count")
    assert resp.status_code == 200

    resp = stub.apis.users.get (srch_type = 'nick_name', srch_txt = '세미콘', sort = "comment_count")
    assert resp.status_code == 200
