idx = 'idx'
subject = 'subject'
writer = 'writer'
sympathy = 'sympathy'
not_sympathy = 'not_sympathy'
reg_time = 'reg_time'

test_table_data = [{
        idx: '0',
        subject: '''땅값 1위 명동 '네이처리퍼블릭' 2억 목전…주요 상권 보유세 부담''',
        writer: '헤드라인',
        sympathy: '100',
        not_sympathy: '30',
        reg_time: '2020-01-02'
    },
    {
        idx: '1',
        subject: '''[단독]3번환자 "자진신고했는데 너무 억울…작년 폐렴때보다 증상미미"''',
        writer: '헤드라인',
        sympathy: '200',
        not_sympathy: '10',
        reg_time: '2020-01-02'
    },
    {
        idx: '2',
        subject: '''손학규 “2선 후퇴 안 해”…바른미래 2차 탈당 움직임''',
        writer: '헤드라인',
        sympathy: '120',
        not_sympathy: '50',
        reg_time: '2020-01-02'
    },
    {
        idx: '3',
        subject: "사진으로 보는 '갤럭시 언팩 2020' …갤럭시Z 플립에 '환호'",
        writer: '헤드라인',
        sympathy: '130',
        not_sympathy: '20',
        reg_time: '2020-01-03'
    },
    {
        idx: '4',
        subject: '''3차귀국 우한교민·중국가족 140명 이천 도착…격리생활 시작(종합)''',
        writer: '헤드라인',
        sympathy: '120',
        not_sympathy: '30',
        reg_time: '2020-01-03'
    },
    {
        idx: '5',
        subject: '''<21대 총선 타깃 여론조사>“범보수 통합신당 지지의향 있다” TK 57.5%·PK 52.3%''',
        writer: '문화일보',
        sympathy: '200',
        not_sympathy: '50',
        reg_time: '2020-01-03'
    },
    {
        idx: '6',
        subject: '''"이기는 공천" "경선 불공평"…한국당 면접 첫날부터 신경전''',
        writer: '연합뉴스',
        sympathy: '200',
        not_sympathy: '30',
        reg_time: '2020-01-03'
    },
    {
        idx: '7',
        subject: ''''패스트트랙 충돌' 민주당측 면책특권 들며 재판서 혐의 부인''',
        writer: '연합뉴스',
        sympathy: '10',
        not_sympathy: '150',
        reg_time: '2020-01-04'
    },
    {
        idx: '8',
        subject: '''안철수 "30% 이상 무당층 우리 바라본다"…창당 23일로 앞당겨''',
        writer: '연합뉴스',
        sympathy: '120',
        not_sympathy: '250',
        reg_time: '2020-01-04'
    },
    {
        idx: '9',
        subject: '''신당 지도부놓고 ‘막판 진통’…한국당서도 ‘黃 2선후퇴’ 주장''',
        writer: '문화일보',
        sympathy: '100',
        not_sympathy: '20',
        reg_time: '2020-01-05'
    },
    {
        idx: '10',
        subject: '''1월 취업자 57만명 늘었지만 51만명은 60대 이상''',
        writer: '연합뉴스',
        sympathy: '130',
        not_sympathy: '530',
        reg_time: '2020-01-05'
    },
    {
        idx: '11',
        subject: '''지난달 취업자 증가…5년 5개월 만 최대''',
        writer: 'MBC',
        sympathy: '120',
        not_sympathy: '350',
        reg_time: '2020-01-06'
    },
    {
        idx: '12',
        subject: '''3차 우한 전세기 국내 도착…유증상자 성인 5명은 이송''',
        writer: 'KBS',
        sympathy: '80',
        not_sympathy: '330',
        reg_time: '2020-01-06'
    },
    {
        idx: '13',
        subject: '''금융위, DLF 제재 오늘 첫 논의…우리·하나銀 운명은?''',
        writer: 'SBS CNBC',
        sympathy: '120',
        not_sympathy: '70',
        reg_time: '2020-01-07'
    },
    {
        idx: '14',
        subject: '''라임 “환매 당시 상환계획 못 지켜…TRS 먼저 갚아야”''',
        writer: 'SBS CNBC',
        sympathy: '190',
        not_sympathy: '130',
        reg_time: '2020-01-07'
    },
    {
        idx: '15',
        subject: '''정권 수사 막으려고… 앞장서 法 뭉개는 법무장관''',
        writer: '문화일보',
        sympathy: '130',
        not_sympathy: '250',
        reg_time: '2020-01-08'
    },
    {
        idx: '16',
        subject: '''[단독]3번환자 "자진신고했는데 너무 억울…작년 폐렴때보다 증상미''',
        writer: '뉴스1',
        sympathy: '100',
        not_sympathy: '320',
        reg_time: '2020-01-08'
    },
    {
        idx: '17',
        subject: '''유증상 교민 1명·배우자 3차전세기 탑승 못해…147명 귀국(종합)''',
        writer: '뉴스1',
        sympathy: '130',
        not_sympathy: '590',
        reg_time: '2020-01-09'
    },
    {
        idx: '18',
        subject: '''정부 "마스크·손소독제 매점매석 업자, 징역형 적극 검토" ''',
        writer: '노컷뉴스',
        sympathy: '120',
        not_sympathy: '130',
        reg_time: '2020-01-09'
    },
    {
        idx: '19',
        subject: '''"4년간 전국 우체국 680곳 없앤다고?" 반발 거세''',
        writer: '오마이뉴스',
        sympathy: '130',
        not_sympathy: '290',
        reg_time: '2020-01-10'
    },
    {
        idx: '20',
        subject: '''전도연, "이제 더이상 '칸의 여왕'이라 부르지 마라" ''',
        writer: '중앙일보',
        sympathy: '130',
        not_sympathy: '30',
        reg_time: '2020-01-11'
    },
    {
        idx: '21',
        subject: '일 크루즈선에 한국인 14명 탑승_미 승객 "내보내 달라"',
        writer: 'YTN',
        sympathy: '120',
        not_sympathy: '10',
        reg_time: '2020-01-12'
    },
    {
        idx: '22',
        subject: ''''기생충' 재개봉에 "반갑다, 다시 보자"…북미·영국도 상영관 확대''',
        writer: 'MBN',
        sympathy: '280',
        not_sympathy: '130',
        reg_time: '2020-01-13'
    },
    {
        idx: '23',
        subject: '''기생충 앞에 중국 '한한령' 무너지나…일본에선 "납득하냐" 설문도''',
        writer: 'MBN',
        sympathy: '910',
        not_sympathy: '150',
        reg_time: '2020-01-14'
    },
    {
        idx: '24',
        subject: '''기생충에 너도나도 숟가락…강효상 "대구에 봉준호 박물관"''',
        writer: '중앙일보',
        sympathy: '120',
        not_sympathy: '600',
        reg_time: '2020-01-15'
    },
    {
        idx: '25',
        subject: '''‘기생충’ 북미 박스오피스 4위로 껑충…오스카 효과 통했다''',
        writer: '한겨레',
        sympathy: '230',
        not_sympathy: '10',
        reg_time: '2020-01-15'
    },
    {
        idx: '26',
        subject: '''“오스카 휩쓴 ‘기생충’ 보러 왔어요”…美 상영관도 역주행''',
        writer: 'KBS',
        sympathy: '300',
        not_sympathy: '20',
        reg_time: '2020-01-15'
    },
    {
        idx: '27',
        subject: '''오락가락·엇박자…사태 키운 ‘日 크루즈 대책’''',
        writer: 'KBS',
        sympathy: '800',
        not_sympathy: '30',
        reg_time: '2020-01-15'
    },
    {
        idx: '28',
        subject: '''중국 전문가 "신종코로나 2월말 절정·4월 마무리 희망"(종합) 포토''',
        writer: '연합뉴스',
        sympathy: '700',
        not_sympathy: '10',
        reg_time: '2020-01-16'
    },
    {
        idx: '29',
        subject: '''中 “코로나 19, 4월 쯤 종료”…후베이성 변수''',
        writer: '연합뉴스',
        sympathy: '920',
        not_sympathy: '10',
        reg_time: '2020-01-16'
    },
    {
        idx: '30',
        subject: '''바이든·워런 동반부진…뉴햄프셔 경선서 대의원 '0명' 전망''',
        writer: '뉴시스',
        sympathy: '820',
        not_sympathy: '150',
        reg_time: '2020-01-16'
    },
    {
        idx: '31',
        subject: '''KAIST, 초고속·초정밀 펄스 비행시간 측정 센서 개발''',
        writer: '연합뉴스',
        sympathy: '800',
        not_sympathy: '190',
        reg_time: '2020-01-16'
    },
    {
        idx: '32',
        subject: '''한 번에 5명 전송... 삼성 '퀵 셰어', 애플 '에어드롭’ 대항마 될까''',
        writer: '조선비즈',
        sympathy: '710',
        not_sympathy: '250',
        reg_time: '2020-01-17'
    },
    {
        idx: '33',
        subject: '''타다, 쏘카에서 독립 "투자 유치…유니콘 도전" ''',
        writer: '파이낸셜뉴스',
        sympathy: '300',
        not_sympathy: '250',
        reg_time: '2020-01-17'
    },
    {
        idx: '34',
        subject: ''''MWC 엑소더스' 인텔·페북마저…개최 취소되나 ''',
        writer: '머니투데이',
        sympathy: '720',
        not_sympathy: '510',
        reg_time: '2020-01-17'
    },
    {
        idx: '35',
        subject: '''팔았다 하면 금세 매진...마스크, 어느 홈쇼핑서 팔까''',
        writer: 'ZDNet Korea',
        sympathy: '320',
        not_sympathy: '150',
        reg_time: '2020-01-18'
    },
    {
        idx: '36',
        subject: '''일본 크루즈선 내 한국인 14명.."국내 이송요청 없어"''',
        writer: '연합뉴스',
        sympathy: '100',
        not_sympathy: '30',
        reg_time: '2020-01-18'
    },
    {
        idx: '37',
        subject: '''코로나19 여파 혈액 수급 '비상'..보유량 고작 3.6일분''',
        writer: '뉴시스',
        sympathy: '200',
        not_sympathy: '10',
        reg_time: '2020-01-18'
    },
    {
        idx: '38',
        subject: '''타다, 쏘카 품 떠난다.."투자유치·사업확대 목표" ''',
        writer: '뉴스1',
        sympathy: '120',
        not_sympathy: '50',
        reg_time: '2020-01-18'
    },
    {
        idx: '39',
        subject: '''바이든·워런 동반부진..뉴햄프셔 경선서 대의원 '0명' 전망''',
        writer: '뉴시스',
        sympathy: '130',
        not_sympathy: '20',
        reg_time: '2020-01-19'
    },
    {
        idx: '40',
        subject: '''3번환자 "자진신고했는데 너무 억울..작년 폐렴때보다 증상미미"''',
        writer: '뉴스1',
        sympathy: '120',
        not_sympathy: '30',
        reg_time: '2020-01-19'
    },
    {
        idx: '41',
        subject: ''''패스트트랙 충돌' 민주당측 면책특권 들며 재판서 혐의 부인''',
        writer: '연합뉴스',
        sympathy: '200',
        not_sympathy: '50',
        reg_time: '2020-01-20'
    },
    {
        idx: '42',
        subject: '''1월 취업자 57만명 늘었지만 51만명은 60대 이상''',
        writer: '연합뉴스',
        sympathy: '200',
        not_sympathy: '30',
        reg_time: '2020-01-20'
    },
    {
        idx: '43',
        subject: '''법인세 5%p 인하, 종부세 완화..한국당, 경제 공약 발표''',
        writer: '뉴스1',
        sympathy: '10',
        not_sympathy: '150',
        reg_time: '2020-01-21'
    },
    {
        idx: '44',
        subject: '''마스크 판매 신고 안하면 최대 징역 2년·벌금 5000만원..실...''',
        writer: '머니투데이',
        sympathy: '120',
        not_sympathy: '250',
        reg_time: '2020-01-21'
    },
    {
        idx: '45',
        subject: '''버티던 MWC, 신종 코로나에 굴복하나..조직위, 취소여부 논의...''',
        writer: '뉴스1',
        sympathy: '100',
        not_sympathy: '20',
        reg_time: '2020-01-22'
    },
    {
        idx: '46',
        subject: '''정부 "아산·진천 교민 700명 추가 조치 없이 15∼16일 퇴...''',
        writer: '연합뉴스',
        sympathy: '130',
        not_sympathy: '530',
        reg_time: '2020-01-22'
    },
    {
        idx: '47',
        subject: '''아직도 안 만난 황교안·유승민..회동 성사 가능할까''',
        writer: '연합뉴스',
        sympathy: '120',
        not_sympathy: '350',
        reg_time: '2020-01-23'
    },
    {
        idx: '48',
        subject: '''그린벨트에 카페 허가했다가 철거 요구한 구청, 소송 패소''',
        writer: '연합뉴스',
        sympathy: '80',
        not_sympathy: '330',
        reg_time: '2020-01-23'
    },
    {
        idx: '49',
        subject: '''홍준표‧김태호 '험지 갈등'..'무소속연대' 뜨나''',
        writer: '노컷뉴스',
        sympathy: '120',
        not_sympathy: '70',
        reg_time: '2020-01-24'
    },
    {
        idx: '50',
        subject: "[단독]'손학규 사퇴 실패' 호남통합 결렬 수순..바른미래 '2차 탈당'",
        writer: '이데일리',
        sympathy: '190',
        not_sympathy: '130',
        reg_time: '2020-01-24'
    },
    {
        idx: '51',
        subject: "김두관·홍준표 양산을 격돌 전망..전직 도지사간 '낙동강 혈투'",
        writer: '연합뉴스',
        sympathy: '130',
        not_sympathy: '250',
        reg_time: '2020-01-25'
    },
    {
        idx: '52',
        subject: '''홍준표 "양산을 김태호에 제의해놓고..당이 날 바보취급"''',
        writer: '뉴스1',
        sympathy: '100',
        not_sympathy: '320',
        reg_time: '2020-01-25'
    },
    {
        idx: '53',
        subject: '''조국, 추미애에게 '박수'.."수사·기소 분리 의미 있는 시도"''',
        writer: '연합뉴스',
        sympathy: '130',
        not_sympathy: '590',
        reg_time: '2020-01-26'
    },
    {
        idx: '54',
        subject: '''달라진 현대차 노조 "고객 없으면 노조도 없다"..생산 만회 호소''',
        writer: '연합뉴스',
        sympathy: '120',
        not_sympathy: '130',
        reg_time: '2020-01-26'
    },
    {
        idx: '55',
        subject: '''기념관·생가·동상..한국당 '봉준호 마케팅' 점입가경''',
        writer: '한겨레',
        sympathy: '130',
        not_sympathy: '290',
        reg_time: '2020-01-27'
    },
    {
        idx: '56',
        subject: '''민중당 "안철수, 3년째 쓰고 있는 주황색 가로챘다"..안 측 "우린 오렌지색"''',
        writer: '경향신문',
        sympathy: '130',
        not_sympathy: '30',
        reg_time: '2020-01-27'
    },
    {
        idx: '57',
        subject: '''외교부 "지소미아 종료는 잠정조치"..日수출규제 철회 다시 촉구''',
        writer: '연합뉴스',
        sympathy: '120',
        not_sympathy: '10',
        reg_time: '2020-01-28'
    },
    {
        idx: '58',
        subject: '''"잠들면 관계 요구"..장문복, 여친 폭로에 "선 넘었다"''',
        writer: '20대 여성',
        sympathy: '280',
        not_sympathy: '130',
        reg_time: '2020-01-28'
    },
    {
        idx: '59',
        subject: '''신천지 교주와 '사실혼' 김남희 씨, 교주 실체 폭로나선 배경은 ?''',
        writer: '30대 여성',
        sympathy: '910',
        not_sympathy: '150',
        reg_time: '2020-01-29'
    },
    {
        idx: '60',
        subject: '''미국서 반려견 핏불이 일가족 4명 물어 1명 사망''',
        writer: '40대 여성',
        sympathy: '120',
        not_sympathy: '600',
        reg_time: '2020-01-30'
    },
    {
        idx: '61',
        subject: '''코로나 TF 팀장 "확진자 28명 상태? 최악이 몸살 기운 정도"''',
        writer: '50대 여성',
        sympathy: '230',
        not_sympathy: '10',
        reg_time: '2020-01-30'
    },
    {
        idx: '62',
        subject: '''[현장영상] 중사본 "신종 코로나 공식 명칭 '코로나19'"''',
        writer: '20대 남성',
        sympathy: '300',
        not_sympathy: '20',
        reg_time: '2020-01-31'
    },
    {
        idx: '63',
        subject: '''조명래 장관 "中 공장 멈춰 하늘 맑아졌다? 근거 없다"''',
        writer: '30대 남성',
        sympathy: '800',
        not_sympathy: '30',
        reg_time: '2020-02-01'
    },
    {
        idx: '64',
        subject: '''여의도 증권사 건물 식당서 칼부림..2명 중상(종합)''',
        writer: '40대 남성',
        sympathy: '700',
        not_sympathy: '10',
        reg_time: '2020-02-02'
    },
    {
        idx: '65',
        subject: '''외교부 "지소미아 종료는 잠정조치"..日수출규제 철''',
        writer: '50대 남성',
        sympathy: '920',
        not_sympathy: '10',
        reg_time: '2020-02-02'
    }
]