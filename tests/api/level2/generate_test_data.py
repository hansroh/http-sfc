#! /usr/bin/env python3

import sqlphile as sp
import mock
from datetime import datetime, timedelta
import random
import mock
import time

UID = "UNITTEST-CONTENT" # semiconexample
IDX_SEMICON = 1302
IDX = 1
PHONE = '01055553333'
IPSUM = '''별을 불러 아이들의 노루, 것은 추억과 딴은 거외다. 이름과 언덕 보고, 흙으로 없이 때 버리었습니다. 가을로 가득 마디씩 토끼, 덮어 이름자 다 밤이 북간도에 봅니다. 비둘기, 그리고 내 듯합니다. 잔디가 이름을 차 거외다.

동경과 까닭이요, 나는 자랑처럼 피어나듯이 노루, 별 이네들은 지나고 듯합니다. 내린 잔디가 별에도 이국 같이 경, 하나에 별을 버리었습니다. 지나고 이름과, 쉬이 별 잠, 소녀들의 가을로 버리었습니다. 하나에 별빛이 하나에 이름과, 까닭입니다. 잔디가 하나에 슬퍼하는 불러 자랑처럼 밤을 지나가는 동경과 계십니다.

마리아 아스라히 그리고 둘 불러 지나고 있습니다. 노새, 아스라히 시인의 말 사람들의 소녀들의 별 있습니다. 언덕 이름과, 피어나듯이 다 듯합니다. 옥 강아지, 노새, 부끄러운 위에도 거외다. 당신은 하늘에는 풀이 잔디가 둘 듯합니다. 새겨지는 새워 이름과, 말 거외다. 프랑시스 하나에 사랑과 헤는 하나의 버리었습니다.'''

SITE_THUMBNAIL_UID = 'VPnYIkkbTVVNMpKNZia0J1'
SITE_URL = 'https://www.yna.co.kr/view/AKR20200219076900004'
DBAUTH = ("example_sandbox", "example", "##########", "80.example.co.kr")

def articles ():
    with sp.pg2.open (*DBAUTH) as db:
        user = db.select ('users').filter (uid = UID, idx = IDX).execute ().fetch ()
        if not user:
            db.insert ('users').data (idx = IDX, uid = UID, phone_no = PHONE)
            db.insert ('rewards').data (user_idx =IDX)

        db.delete ('user_actions').filter (
            content_idx__in = db.select ('contents').filter (user_idx = IDX).get ('idx')
        ).execute ()
        db.delete ('comments').filter (content_idx__in = db.select ('contents').filter (user_idx = IDX).get ('idx')).execute ()
        db.delete ('site_thumbnail').filter (user_idx = IDX).execute ()
        db.delete ('contents').filter (user_idx = IDX).execute ()
        db.commit ()

        for rownum, art in enumerate (mock.test_table_data):
            if rownum == 20:
                break
            main_text = '<p>' + IPSUM + '</p>'
            row = (db.insert ('contents')
                        .data (
                            user_idx = IDX,
                            reg_date_time = datetime.now (),
                            subject = art ['subject'],
                            main_text = main_text,
                            category_idx = random.choice ([1, 1, 1,2, 3, 4]),
                            nick_name = '관리자',
                            status = 1,
                            disp_start = datetime.now (),
                            disp_end = datetime.now () + timedelta (days = random.randrange (-30, 30))
                        )
                    ).returning ('idx').execute ()

        (db.update ('users').filter (uid = UID)
                .data (
                    nick_name = '세미콘네트웍스', provider='google.com', email = 'hrroh@example.co.kr',
                    reg_date_time = datetime.now (), last_updated = datetime.now (),
                    lev = 'user'
                )).execute ()

        (db.insert ('site_thumbnail')
                .data (
                    description = "[게시판] 21일까지 경복궁역에서 가습기살균제참사 진상규명 전시회, 박의래기자, 사회뉴스 (송고시간 2020-02-19 11:22)",
                    user_idx = IDX,
                    image = "/media/articles/scraper/VPnYIkkbTVVNMpKNZia0Jw.jpe",
                    url = SITE_URL,
                    title = '[게시판] 21일까지 경복궁역에서 가습기살균제참사 진상규명 전시회 | 연합뉴스',
                    site_name = '연합뉴스',
                    locale='ko_KR',
                    image_width=500,
                    image_height=280,
                    uid=SITE_THUMBNAIL_UID,
                    canonical_uid='VPnYIkkbTVVNMpKNZia0J2'
                )
            ).execute ()

        for art in mock.test_table_data:
            main_text = '<p>' + IPSUM + '</p>'
            comment_count = random.randrange (20)
            syms, nsyms, reports = random.randrange (20), random.randrange (20), random.randrange (20)

            row = (db.insert ('contents')
                .data (
                    user_idx = IDX,
                    news_url = SITE_URL,
                    site_thumbnail_uid = SITE_THUMBNAIL_UID,
                    reg_date_time = datetime.now (),
                    subject = art ['subject'],
                    main_text = main_text,
                    category_idx = random.choice ([2,3,4]),
                    nick_name = '세미콘 네트웍스',
                    sympathy = syms,
                    not_sympathy = nsyms,
                    status = random.choice ([0,1,1,1,1,1,1,1,1,9]),
                    comment_count = comment_count,
                    report_count = reports
                )
            ).returning ('idx').execute ().one ()

            for i in range (syms):
                db.insert ('user_actions').data (user_idx = random.choice ([None, IDX, IDX_SEMICON]), content_idx = row.idx, action = 1, issued_reward_point = random.randrange (2)).execute ()
            for i in range (nsyms):
                db.insert ('user_actions').data (user_idx = random.choice ([None, IDX, IDX_SEMICON]), content_idx = row.idx, action = 2, issued_reward_point = random.randrange (2)).execute ()
            for i in range (reports):
                db.insert ('user_actions').data (user_idx = random.choice ([None, IDX, IDX_SEMICON]), content_idx = row.idx, action = 9).execute ()

            for i in range (comment_count):
                syms, nsyms, reports = random.randrange (20), random.randrange (20), random.randrange (20)
                cmt = (db.insert ('comments')
                    .data (
                        user_idx = IDX,
                        content_idx = row.idx,
                        nick_name = '세미콘 네트웍스',
                        text = '{}번째 댓글입니다'.format (i + 1),
                        sympathy = syms,
                        not_sympathy = nsyms,
                        report_count = reports,
                        status = random.choice ([0,1,1,1,1,1,1,1,1,9])
                    )
                ).returning ('idx').execute ().one ()

                for i in range (syms):
                    db.insert ('user_actions').data (user_idx = random.choice ([None, IDX, IDX_SEMICON]), content_idx = row.idx, comment_idx = cmt.idx, action = 1, issued_reward_point = random.randrange (2)).execute ()
                for i in range (nsyms):
                    db.insert ('user_actions').data (user_idx = random.choice ([None, IDX, IDX_SEMICON]), content_idx = row.idx, comment_idx = cmt.idx, action = 2, issued_reward_point = random.randrange (2)).execute ()
                for i in range (reports):
                    db.insert ('user_actions').data (user_idx = random.choice ([None, IDX, IDX_SEMICON]), content_idx = row.idx, comment_idx = cmt.idx, action = 9).execute ()

            db.commit ()
            time.sleep (0.01)

        db.commit ()

def qna ():
    with sp.pg2.open (*DBAUTH) as db:
        db.delete ('qna').filter (user_idx = IDX).execute ()
        for i in range (40):
            q = (db.insert ('qna')
                .data (
                    user_idx = IDX, manager_idx = IDX,
                    q_title = '{}. 이 프로젝트 언제 끝나요?'.format (i),
                    q_text = '<p>' + IPSUM + '</p>',
                ))
            status = random.randrange (3)
            if status == 2:
                q.data (
                    a_title = '{}. 미안하지만 이 프로젝트는 끝나지 않습니다'.format (i),
                    a_text = '<p>' + IPSUM + '</p>',
                    a_time = datetime.now (),
                    status = status
                )
            else:
                q.data (status = status)
            q.execute ()
            time.sleep (0.1)
        db.commit ()


def rewards ():
    from uuid import uuid4
    from pprint import pprint
    from datetime import date

    payload = {
        'address': '0xe56f7252225790204408d80213591e2b994ee6df',
        'app_name': 'TAEKBAE',
        'app_version': '0.12',
        'area': None,
        'balance': 50,
        'birth': date(1966, 10, 23),
        'conn_id': 'null',
        'device_key': 'db072a313dba5d7b',
        'dup_id': '',
        'email': '4578kjy@gmail.com',
        'gender': '1',
        'ident_dump': '',
        'ident_number': 'MBH446202003031487460826',
        'mobile_co': 'KTF',
        'mobile_no': '01055553333',
        'name': '홍길동',
        'national': '0',
        'pin': '111111',
        'uuid': str (uuid4 ())
    }

    BL_DBAUTH = ('example_sandbox',) + DBAUTH [1:]
    with sp.pg2.open (*BL_DBAUTH) as db:
        wallet = db.select ('wallet').get ('id').filter (mobile_no = PHONE).execute ().fetch ()
        if not wallet:
            wallet_idx = db.insert ('wallet').data (**payload).returning ('id').execute ().one ().id
        else:
            wallet_idx = wallet [0].id
        db.commit ()

    with sp.pg2.open (*DBAUTH) as db:
        (db.delete ('point_transfer_log').filter (order_idx__in = db.select ('point_transfer_order').get ('idx').filter (wallet_idx = wallet_idx))).execute ()
        (db.delete ('point_transfer_order').filter (wallet_idx = wallet_idx)).execute ()

        for i in range (50):
            q = (db.insert ('point_transfer_order')
                    .data (
                        user_idx = IDX,
                        amount = random.randrange (10, 100),
                        wallet_idx = wallet_idx
                    ))

            status = random.choice (['pending', 'queued', 'processing', 'success', 'failed', 'verifying'])
            if status in ['processing', 'verifying', 'success', 'failed']:
                order_idx = q.data (
                    last_updated = datetime.now (),
                    block_number = 100,
                    tx_id = '<TX_ID>'
                ).returning ('idx').execute ().one ().idx

                db.insert ('point_transfer_log').data (
                    status = 'pending',
                    order_idx = order_idx
                ).execute ()

                for i in range (random.randrange (3)):
                    status = random.choice (['queued', 'processing', 'success', 'failed', 'verifying'])
                    db.insert ('point_transfer_log').data (
                        status = status,
                        order_idx = order_idx
                    ).execute ()
                    if status in ('failed', 'success'):
                        break

                (db.update ('point_transfer_order')
                        .data (status = status, last_updated = datetime.now ())
                        .filter (idx = order_idx)).execute ()

        db.commit ()


if __name__ == '__main__':
    articles ()
    qna ()
    rewards ()

