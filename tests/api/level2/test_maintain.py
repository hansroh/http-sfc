import pytest
from pprint import pprint
from generate_test_data import IDX, UID, SITE_URL

def test_maintain (stub, exister, csrf):

    resp = exister (UID, 'user', auth = True)

    resp = stub.apis.maintain.articles (10000000).delete ()
    assert resp.status_code == 403

    resp = stub.apis.maintain.users (10000000).delete ()
    assert resp.status_code == 403

    resp = exister (UID, 'staff', auth = True)

    resp = stub.apis.maintain.articles (10000000).delete ()
    assert resp.status_code == 201

    resp = stub.apis.maintain.users (10000000).delete ()
    assert resp.status_code == 201
