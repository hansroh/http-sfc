import pytest
from pprint import pprint
from generate_test_data import IDX, UID, SITE_URL

payload = dict (
    val = 1
)

payload2 = dict (
    text = "Test"
)

def test_configs (stub, exister, csrf):
    resp = stub.apis.configs.get ()
    assert resp.status_code == 401
    resp = exister (UID, 'user', auth = True)
    resp = stub.apis.configs.get ()
    assert resp.status_code == 403
    resp = exister (UID, 'staff', auth = True)
    resp = stub.apis.configs.get ()
    assert resp.status_code == 200
    assert 'configs' in resp.data

    resp = stub.apis.configs ('test').put (payload)
    assert resp.status_code == 201

    resp = stub.apis.configs.get ()
    assert resp.status_code == 200
    has_test = False
    for each in resp.data ['configs']:
        if 'item' in each:
            has_test = True
            break
    assert has_test

    resp = stub.apis.configs ('test').get ()
    assert resp.status_code == 200
    assert resp.data ['val'] == 1

    resp = stub.apis.configs ('test').delete (val = -1)
    assert resp.status_code == 201

    resp = stub.apis.configs ('test').get ()
    assert resp.status_code == 410

    # test text
    resp = stub.apis.configs ('test').put (payload2)
    assert resp.status_code == 201

    resp = stub.apis.configs.get ()
    assert resp.status_code == 200
    has_test = False
    for each in resp.data ['configs']:
        if 'item' in each:
            has_test = True
            break
    assert has_test

    resp = stub.apis.configs ('test').get ()
    assert resp.status_code == 200
    assert resp.data ['text'] == 'Test'

    resp = stub.apis.configs ('test').delete (val = -1)
    assert resp.status_code == 201

    resp = stub.apis.configs ('test').get ()
    assert resp.status_code == 410

    resp = stub.apis.configs.get (names = "contents|comments")
    assert resp.status_code == 200

    resp = stub.apis.configs ('policy').get ()
    assert resp.status_code == 200

