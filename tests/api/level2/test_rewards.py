import pytest
from pprint import pprint
from generate_test_data import IDX, UID, IPSUM
from datetime import datetime, timedelta
import requests
import time

payload = dict ()

def test_rewards (stub, exister, csrf):
    resp = stub.apis.users ('me').rewards.get ()
    assert resp.status_code == 401

    resp = exister (UID, 'user', auth = True)

    resp = stub.apis.users ('me').rewards.get ()
    assert resp.status_code == 200
    assert 'orders' in resp.data
    assert 'stat' in resp.data
    balance = resp.data ['stat']['amount']

    resp = stub.apis.users ('me').rewards.orders.post ({'amount': 10**12})
    assert resp.status_code == 428

    resp = stub.apis.users ('me').rewards.orders.post ({'amount': 1})
    assert resp.status_code == 201
    idx = resp.data ['url'].split ('/')[-1]

    resp = stub.apis.users ('me').rewards.get ()
    assert balance == resp.data ['stat']['amount'] + 1

    resp = stub.apis.users ('me').rewards.orders (idx).get ()
    assert resp.status_code == 200
    amount = resp.data ['order']['amount']

    resp = stub.apis.rewards.orders (idx).logs.post ({'status': 'sad'})
    assert resp.status_code == 400

    resp = stub.apis.rewards.orders (idx).logs.post ({'status': ''})
    assert resp.status_code == 400

    resp = stub.apis.rewards.orders (idx).logs.post ({'status': 'success'})
    assert resp.status_code == 201

    resp = stub.apis.users ('me').rewards.orders (idx).get ()
    resp.data ['order']['status'] == 'success'

    resp = stub.apis.rewards.orders (idx).logs.post ({'status': 'failed'})
    assert resp.status_code == 201

    resp = stub.apis.users ('me').rewards.orders (idx).get ()
    resp.data ['order']['status'] == 'failed'

    resp = stub.apis.users ('me').rewards.get ()
    assert balance == resp.data ['stat']['amount']

    resp = requests.get ('https://example.co.kr/apis/wallets?number=01055553333')
    wallet_uuid = resp.json ()['uuid']

    resp = requests.get ('https://example.co.kr/apis/wallets/{}/transactions'.format (wallet_uuid), headers = {'X-Pin': '1111'})
    assert resp.status_code == 401

    resp = requests.get ('https://example.co.kr/apis/wallets/{}/transactions'.format (wallet_uuid), headers = {'X-Pin': '111111'})
    lastet = resp.json () ['txs'][0]

    assert lastet ['related'] == '공비'
    assert lastet ['amount'] == 1
    assert lastet ['detail'] == 'TransferTx'
    assert time.time () - lastet ['timestamp'] < 10


def test_rewards_list (stub, exister, csrf):
    resp = stub.apis.rewards.get ()
    assert resp.status_code == 401
    resp = exister (UID, 'staff', auth = True)

    resp = stub.apis.rewards.get (uid = UID)
    assert resp.status_code == 200
    assert 'record_count' in resp.data
    assert 'users' in resp.data

    for user in resp.data ['users']:
        resp = stub.apis.users (user ['uid']).rewards.get ()
        assert resp.status_code == 200
