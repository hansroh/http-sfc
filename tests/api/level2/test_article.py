import pytest
from pprint import pprint
from generate_test_data import IDX, UID, SITE_URL
from copy import copy
import time

ARTICLE = None

def test_article (stub):
    global ARTICLE
    resp = stub.apis.articles.get (srch_type='uid', srch_txt = UID, category = 2)
    assert resp.status_code == 200
    assert resp.data ['record_count']
    assert resp.data ['item_list']
    first = None
    for item in resp.data ['item_list']:
        if not item ['disp_end']:
            first = item
            break
    resp = stub.articles (first ['idx']).get ()
    ARTICLE = resp.data
    assert 'main_text' in resp.data
    assert 'item' in resp.data
    assert 'site_thumbnail' in resp.data
    assert 'category' in resp.data

    resp = stub.apis.articles (first ['idx']).get ()
    assert resp.status_code == 200
    assert 'main_text' in resp.data
    assert 'item' in resp.data
    assert 'site_thumbnail' in resp.data
    assert 'category' in resp.data


def test_article_modify (stub, exister, csrf):
    payload = dict (
        subject = ARTICLE ['item']['subject'],
        main_text = '수정됨\n' + ARTICLE ['main_text'],
        category_idx = 2,
        news_url = SITE_URL
    )
    resp = stub.apis.articles (ARTICLE ['item']['idx']).patch (payload)
    assert resp.status_code == 401

    resp = exister (UID, 'user', auth = True)
    resp = stub.apis.articles (0).patch (payload)
    assert resp.status_code == 400

    resp = stub.apis.composer.scraper.post ({"url": SITE_URL})
    assert resp.status_code == 200
    if 'callback' in resp.data:
        thumbnail_uid = resp.data ['callback'].split ('/') [-1]
        resp = stub.apis.composer.scraper (thumbnail_uid).put (resp.data ['item'])

    resp = stub.apis.articles (ARTICLE ['item']['idx']).patch (payload, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201

    payload ['main_text'] == '추가됨'
    resp = stub.apis.articles.post (payload)
    assert resp.status_code == 400

    resp = stub.apis.articles.post (payload, headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 201

    new_idx = resp.data ['url'].split ('/')[-1]

    resp = stub.apis.articles.get ()
    assert resp.status_code == 200
    assert [row for row in resp.data ['item_list'] if row ['idx'] == int (new_idx)]

    resp = stub.apis.articles (new_idx).delete (headers = {'X-XSRF-Token': csrf})
    assert resp.status_code == 204

    resp = stub.apis.articles.get ()
    assert resp.status_code == 200
    for row in resp.data ['item_list']:
        if row ['idx'] == int (new_idx):
            assert row ['status'] == 0
