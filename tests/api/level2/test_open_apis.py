import pytest
from pprint import pprint
from generate_test_data import IDX, UID, SITE_URL
from copy import copy
import time

def test_open_api (stub, exister, csrf):
    resp = stub.apis.users (UID).stat.get ()
    assert 'stat' in resp.data
    assert 'user' in resp.data

    resp = stub.apis.users (UID).stat.get (category = 2)
    assert 'stat' in resp.data
    assert 'user' in resp.data

    resp = stub.apis.users (UID).stat.get (tt = 2)
    assert resp.status_code == 400
