import requests
from pprint import pprint
import pytest
import json

KAKAO = "Zqy0DhWMSESz9ilspTiuErSxq87azevr4RL4ogopcSEAAAFwPFXlzw"
NAVER = "AAAAOtVKpeLseHJoWAfua76DfTv7XFkMXD1wGpfXYIYmEnjIXi9cxfUkQr8WwKYQGLGaL__BRS3zzmPh-ef-Bw9gOKI"

MINOR_OAUTH_PROVIDERS = {
    'naver': 'https://openapi.naver.com/v1/nid/me',
    'kakao': 'https://kapi.kakao.com/v2/user/me'
}

KAKAO_PAYLOAD = {'connected_at': '2020-02-13T02:17:07Z',
 'id': 1281739268,
 'kakao_account': {'birthday': '0307',
                   'birthday_needs_agreement': False,
                   'birthday_type': 'SOLAR',
                   'email': 'test@gmail.kkk',
                   'email_needs_agreement': False,
                   'gender': 'male',
                   'gender_needs_agreement': False,
                   'has_birthday': True,
                   'has_email': True,
                   'has_gender': True,
                   'is_email_valid': True,
                   'is_email_verified': True,
                   'profile': {'nickname': 'shjin',
                               'profile_image_url': 'http://k.kakaocdn.net/dn/qhgP4/btqrZqqQr8a/ctB4sYmmfo18cW89X/img_640x640.jpg',
                               'thumbnail_image_url': 'http://k.kakaocdn.net/dn/qhgP4/btqrZqqQr8a/ctB4sYmmfo18cW89X/img_110x110.jpg'},
                   'profile_needs_agreement': False},
 'properties': {'nickname': 'shjin',
                'profile_image': 'http://k.kakaocdn.net/dn/qhgP4/btqrZqqQr8a/ctB4sYmmfo18cW89X/img_640x640.jpg',
                'thumbnail_image': 'http://k.kakaocdn.net/dn/qhgP4/btqrZqqQr8a/ctB4sYmmfo18cW89X/img_110x110.jpg'}
}

NAVER_PAYLOAD = {'message': 'success',
 'response': {'birthday': '03-07',
              'email': 'test@gmail.kkk',
              'gender': 'M',
              'id': '93167689',
              'name': 'Hans Roh'},
 'resultcode': '00'
}

@pytest.mark.skip
def test_oauth_kakao (engine):
    resp = requests.get (
        MINOR_OAUTH_PROVIDERS ['kakao'],
        headers = {'Authorization': 'Bearer {}'.format (KAKAO)}
    )
    pprint (resp.status_code)
    pprint (resp.json ())

@pytest.mark.skip
def test_oauth_naver (engine):
    resp = requests.get (
        MINOR_OAUTH_PROVIDERS ['naver'],
        headers = {'Authorization': 'Bearer {}'.format (NAVER)}
    )
    pprint (resp.status_code)
    pprint (resp.json ())

@pytest.mark.skip
def test_custom_token_naver (engine):
    payload = dict (
        provider = 'naver',
        access_token = NAVER
    )
    resp = engine.post ('/apis/auth/firebase_custom_token', payload)
    pprint (resp.data)
    assert resp.status_code == 200

@pytest.mark.skip
def test_custom_token_kakao (engine):
    payload = dict (
        provider = 'kakao',
        access_token = KAKAO
    )
    resp = engine.post ('/apis/auth/firebase_custom_token', payload)
    assert resp.status_code == 200

@pytest.mark.skip
def test_test_custom_token_error (engine):
    payload = dict (
        provider = 'unknown',
        access_token = KAKAO
    )
    resp = engine.post ('/apis/auth/firebase_custom_token', payload)
    assert resp.status_code == 400

    payload = dict (
        provider = 'kakao',
        access_token = KAKAO + 'b'
    )
    resp = engine.post ('/apis/auth/firebase_custom_token', payload)
    assert resp.status_code == 400

def test_custom_token_naver_from_paylod (engine, stub):
    payload = dict (
        provider = 'naver',
        access_token = NAVER,
        payload = NAVER_PAYLOAD
    )
    resp = stub.apis.auth.firebase_custom_token.post (payload)
    if resp.status_code == 510:
        return
    assert resp.status_code == 200

    assert resp.data ['custom_token']
    assert resp.data ['profile']
    pprint (resp.data)

    payload = dict (
        provider = 'kakao',
        access_token = KAKAO,
        payload = KAKAO_PAYLOAD
    )
    resp = stub.apis.auth.firebase_custom_token.post (payload)
    assert resp.status_code == 200
    assert resp.data ['custom_token']
    assert resp.data ['profile']
