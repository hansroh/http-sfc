import pytest
from pprint import pprint
import time

ID_TOKEN = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjYzZTllYThmNzNkZWExMTRkZWI5YTY0OTcxZDJhMjkzN2QwYzY3YWEiLCJ0eXAiOiJKV1QifQ.eyJlbWFpbCI6ImJhbmc0d29uMzdAbmF2ZXIuY29tIiwiZ2VuZGVyIjoiTSIsIm5pY2tuYW1lIjoiIiwicHJvZmlsZV9pbWFnZSI6IiIsImJpcnRoZGF5IjoiMDMwNyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9nb25nZ2FtIiwiYXVkIjoiZ29uZ2dhbSIsImF1dGhfdGltZSI6MTU4MTU5NzQxMiwidXNlcl9pZCI6IkZLVmppZHdrN3N1cllRMEJ6WGgwdEMxamRYTjAiLCJzdWIiOiJGS1ZqaWR3azdzdXJZUTBCelhoMHRDMWpkWE4wIiwiaWF0IjoxNTgxNTk3NDEyLCJleHAiOjE1ODE2MDEwMTIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnt9LCJzaWduX2luX3Byb3ZpZGVyIjoiY3VzdG9tIn19.w1pv8_Wr1SlyMZnrQr9gU3jXQvGsYPuWUSghDBa9ma7HKmpEm8y9tFZbcaHOY5bnkG6liuNtyQuSC-RkXiOqSX21sUIfJwV6WZYfDefumIuHmADhkHhXJ4-L6jNkHVRZUUKQbjuqz8a2xZ1_Ug0PqsQU5-eypq6OmKQCW7haFB7svjYUx2mpVpCzRjd6PLj4yGk_jeFHcnAHSDr17UyKhzDnjWfdJqEnynXcCbQIPbpxgU3sgKK1sN-JCx4zEdC5sMFHP1lN8Ywv9gVeoZs-CltDp7eo2YAjZhPkGnQM6jFlbV6cUsQQDpY3uQZoCDsG326ygTk9ge9v0vju-3SLvw"

@pytest.mark.skip
def test_login_with_firebase_id_token_error (engine, stub):
    payload = dict (
        id_token = ID_TOKEN + 'b',
        profile = {}
    )
    resp = stub.apis.auth.login_with_firebase_id_token.post (payload)
    assert resp.status_code == 502
    assert resp.data ['code']

@pytest.mark.skip
def test_login_with_firebase_id_token (engine, stub):
    payload = dict (
        id_token = ID_TOKEN,
        profile = {}
    )
    resp = stub.apis.auth.login_with_firebase_id_token.post (payload)
    assert resp.status_code == 200
    assert 'nick_name' in resp.data
    assert resp.data ['access_token']
    assert resp.data ['refresh_token']

def test_login_with_firebase_id_token_with_decoded_token (engine, stub, loginer):
    resp = stub.apis.users ('me').profile.get ()
    assert resp.status_code == 401

    resp = loginer ('UNITTEST', 'user', {'lev': 'staff'})
    assert resp.status_code == 400

    resp = loginer ('UNITTEST', 'user')
    assert 'nick_name' in resp.data
    stub._set_jwt (resp.data ['access_token'])

def test_profile (engine, stub, loginer):
    UID = 'UNITTEST'

    resp = loginer (UID, 'user')
    assert 'nick_name' in resp.data

    ACCESS_TOKEN = resp.data ['access_token']
    REFRESH_TOKEN = resp.data ['refresh_token']
    stub._set_jwt (ACCESS_TOKEN)

    resp = stub.apis.users ('me').profile.get ()
    assert resp.status_code == 200
    assert resp.data ['profile']['uid'] == UID

    resp = stub.apis.users ('me').open_profile.get ()
    assert resp.status_code == 410

    resp = stub.apis.users (UID).open_profile.get ()
    assert resp.status_code == 200
    assert resp.data ['profile']['uid'] == UID

    resp = stub.apis.users (UID).profile.delete (real = 'true')
    assert resp.status_code == 403

    resp = stub.apis.users (UID).profile.patch (dict (email = 'reported'))
    assert resp.status_code == 403

    resp = stub.apis.auth.nick_names ('PYTEST').put ()
    assert resp.status_code == 201

    resp = stub.apis.users ('me').profile.patch (dict (email = 'reported', nick_name = '테' * 9))
    assert resp.status_code == 400

    resp = stub.apis.users ('me').profile.patch (dict (email = 'reported', nick_name = 'PYTEST'))
    assert resp.status_code == 200

    resp = stub.apis.auth.nick_names ('PYTEST').put ()
    assert resp.status_code == 409

    resp = stub.apis.users (UID).permission.post ({'lev': 'staff'})
    assert resp.status_code == 405

    resp = stub.apis.users (UID).permission.patch ({'lev': 'staff'})
    assert resp.status_code == 403

    resp = stub.apis.users ('me').permission.patch ({'lev': 'staff'})
    assert resp.status_code == 403

    # try with duplicated nickname
    stub._set_jwt ()
    resp = loginer (UID + '-1', 'user', {'nick_name': 'PYTEST'})
    assert resp.status_code == 409

    # restore
    stub._set_jwt (ACCESS_TOKEN)

def test_profile_admin (engine, stub, loginer):
    UID = 'UNITTEST-STAFF'

    resp = loginer (UID, 'staff')
    assert 'nick_name' in resp.data

    ACCESS_TOKEN = resp.data ['access_token']
    REFRESH_TOKEN = resp.data ['refresh_token']
    stub._set_jwt (ACCESS_TOKEN)

    resp = stub.apis.users ('me').profile.get ()
    assert resp.status_code == 200
    assert resp.data ['profile']['uid'] == UID

    resp = stub.apis.users (UID).profile.get ()
    assert resp.status_code == 200
    assert resp.data ['profile']['uid'] == UID

    resp = stub.apis.users (UID).permission.patch ({'status': None, 'lev': 'staff'})
    assert resp.status_code == 200

    resp = stub.apis.users (UID).permission.patch ({})
    assert resp.status_code == 400

    resp = stub.apis.users (UID).permission.patch ({'status': 'resigned'})
    assert resp.status_code == 400

    resp = stub.apis.users (UID).permission.patch ({'status': '스패머', 'lev': 'staff'})
    assert resp.status_code == 200

    resp = stub.apis.users (UID).permission.patch ({'status': None, 'lev': 'staff'})
    assert resp.status_code == 200

    time.sleep (1.0)


def test_token (engine, stub, loginer):
    UID = 'UNITTEST'

    resp = loginer (UID, 'user')
    ACCESS_TOKEN = resp.data ['access_token']
    REFRESH_TOKEN = resp.data ['refresh_token']
    stub._set_jwt (ACCESS_TOKEN)

    resp = stub.apis.auth.access_token.post ({'refresh_token': REFRESH_TOKEN})
    assert resp.status_code == 200
    assert 'access_token' in resp.data
    assert 'nick_name' in resp.data

    resp = stub.apis.auth.refresh_token.post ({'refresh_token': REFRESH_TOKEN})
    assert resp.status_code == 409

    resp = stub.apis.auth.refresh_token.post ({'force': True, 'refresh_token': REFRESH_TOKEN})
    assert resp.status_code == 200
    ACCESS_TOKEN = resp.data ['access_token']
    REFRESH_TOKEN = resp.data ['refresh_token']
    stub._set_jwt (ACCESS_TOKEN)

    resp = stub.apis.auth.access_token.post ({'refresh_token': REFRESH_TOKEN})
    assert resp.status_code == 200
    assert 'access_token' in resp.data
    assert 'nick_name' in resp.data
