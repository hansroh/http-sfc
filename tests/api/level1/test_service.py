
def test_service (engine, stub):
    resp = engine.get ("/")
    assert resp.status_code == 200

    resp = stub.apis.auth.test.get ()
    assert resp.status_code == 201
    assert resp.data ['result'] == 'ok'
