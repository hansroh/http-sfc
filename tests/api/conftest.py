from rs4 import partial, webtest, importer
from skitai.testutil import launcher
import pytest
import requests
import os
import functools
import time
settings = importer.from_file ('settings', os.path.join (os.path.dirname (__file__), '../../pwa/orm/orm/settings.py'))

@pytest.fixture
def engine ():
  launch = partial (launcher.Launcher, port = settings.BACKEND_PORT, ssl = False, silent = True, dry = True)
  engine_ = launch ("../../apiserve/serve.py")
  yield engine_
  engine_.stop ()

@pytest.fixture
def stub (engine):
  return engine.api ()

# login engine ----------------------------------------
def id_token_payload (uid, lev):
    return "eyJhbGciOiJSUzI1NiIsImtpZCI6IjYzZTllYThmNzNkZWExMTRkZWI5YTY0OTcxZDJhMjkzN2QwYzY3YWEiLCJ0eXAiOiJKV1QifQ.eyJlbWFpbCI6ImJhbmc0d29uMzdAbmF2ZXIuY29tIiwiZ2VuZGVyIjoiTSIsIm5pY2tuYW1lIjoiIiwicHJvZmlsZV9pbWFnZSI6IiIsImJpcnRoZGF5IjoiMDMwNyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9nb25nZ2FtIiwiYXVkIjoiZ29uZ2dhbSIsImF1dGhfdGltZSI6MTU4MTU5NzQxMiwidXNlcl9pZCI6IkZLVmppZHdrN3N1cllRMEJ6WGgwdEMxamRYTjAiLCJzdWIiOiJGS1ZqaWR3azdzdXJZUTBCelhoMHRDMWpkWE4wIiwiaWF0IjoxNTgxNTk3NDEyLCJleHAiOjE1ODE2MDEwMTIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnt9LCJzaWduX2luX3Byb3ZpZGVyIjoiY3VzdG9tIn19.w1pv8_Wr1SlyMZnrQr9gU3jXQvGsYPuWUSghDBa9ma7HKmpEm8y9tFZbcaHOY5bnkG6liuNtyQuSC-RkXiOqSX21sUIfJwV6WZYfDefumIuHmADhkHhXJ4-L6jNkHVRZUUKQbjuqz8a2xZ1_Ug0PqsQU5-eypq6OmKQCW7haFB7svjYUx2mpVpCzRjd6PLj4yGk_jeFHcnAHSDr17UyKhzDnjWfdJqEnynXcCbQIPbpxgU3sgKK1sN-JCx4zEdC5sMFHP1lN8Ywv9gVeoZs-CltDp7eo2YAjZhPkGnQM6jFlbV6cUsQQDpY3uQZoCDsG326ygTk9ge9v0vju-3SLvw", {
        'email': 'bang4won37@naver.com',
        'gender': 'M',
        'nickname': '',
        'profile_image': '',
        'birthday': '0307',
        'iss': 'https://securetoken.google.com/example',
        'aud': 'example', 'auth_time': 1581574561,
        'user_id': '14a56389dc24eecbab610d01cd7874b4',
        'sub': '14a56389dc24eecbab610d01cd7874b4',
        'iat': 1581574561, 'exp': 1581578161,
        'firebase': {'identities': {}, 'sign_in_provider': 'custom'},
        'uid': uid,
        'lev': lev
    }

def login_with_firebase_id_token (uid, lev = 'user', profile = None, auth = False, stub = None):
    ID_TOKEN, ID_TOKEN_DECODED = id_token_payload (uid, lev)
    payload = dict (
        id_token = ID_TOKEN,
        profile = profile,
        decoded_token = ID_TOKEN_DECODED
    )
    resp = stub.apis.auth.login_with_firebase_id_token.post (payload)
    auth and stub._set_jwt (resp.data ['access_token'])
    return resp

@pytest.fixture
def loginer (stub):
    yield functools.partial (login_with_firebase_id_token, stub = stub)
    resp = stub.apis.users ('me').profile.delete (real = 'true')
    assert resp.status_code == 201
    stub._set_jwt ()


@pytest.fixture
def exister (stub):
    yield functools.partial (login_with_firebase_id_token, stub = stub)
    stub._set_jwt ()

@pytest.fixture
def csrf (stub):
    return stub.apis.auth.csrf_token.get ().data ['token']
