#! /bin/bash

# if [ "$1" = "" ]
# then
#     echo "visit https://sites.google.com/a/chromium.org/chromedriver/downloads, copy link then"
#     exit 1
# fi

# apt install -y zip unzip libxml2-dev libxmlsec1-dev libpq-dev libblas-dev liblapack-dev gfortran
# wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
# sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
# sudo apt update
# sudo apt -y install google-chrome-stable
# google-chrome --version

# wget $1
# unzip -od drivers chromedriver_linux64.zip
# rm chromedriver_linux64.zip

apt update
apt -y install chromium-chromedriver
pip install -Ur requirements.txt
