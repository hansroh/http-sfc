import os
import pytest
from rs4.webkit import Chrome
from rs4 import webtest

DEVSERVER = "http://localhost:{}".format (os.environ.get ("BACKEND_PORT", "5000"))

class Chrome (Chrome):
  def navigate (self, url):
    return super ().navigate (DEVSERVER + url)

@pytest.fixture
def driver ():
  if os.name == 'nt':
    return Chrome (os.path.join (os.path.dirname (__file__), "drivers", "chromedriver.exe"))
  else:
    return Chrome ("/usr/bin/chromedriver", headless = True)

def login (driver, user = 'hrroh2@example.co.kr', pw = '11111111'):
  driver.navigate ('/apis/auth/login?return_url=%2Farticles%3Fcategory%3D3')
  driver.wait (3)
  email_input = driver.one ("input[type=text]")
  assert email_input, "cnnot find username input"
  email_input.send_keys ("{}\t{}\n".format (user, pw))
  driver.sleep (1)

@pytest.fixture
def user ():
  login ('hrroh2@example.co.kr', '11111111')

