import conftest
import os
import pytest

def test_index (driver):
    with driver:
        driver.navigate ('/')
        driver.wait_until ('visible', 'img[src="/assets/image/icon_home.png"]')
        btn_post = driver.one ("button-text:글쓰기")
        assert btn_post
        driver.capture ()
        assert os.path.exists ('screenshot.jpg')

def test_login (driver):
    user = 'hrroh2@example.co.kr'; pw = '11111111'
    with driver:
        driver.navigate ('/apis/auth/login?return_url=%2Farticles%3Fcategory%3D3')
        email_input = driver.one ("input[type=text]", 'visible')
        assert email_input, "cnnot find username input"
        email_input.send_keys ("{}\t{}\n".format (user, pw))
        driver.one ('img[src="/assets/image/icon_home.png"]', 'visible')
        e = driver.one ('header')
        assert '마이페이지' in e.text
        driver.capture ()
