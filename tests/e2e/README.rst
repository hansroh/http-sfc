
Installing Unit Test
===================================

기본 모듈 설치
-------------------------

.. code:: bash

  sudo ./install.sh


lxml 설치 (Win 32)
-----------------------

파이썬 버젼과 아키텍처(32/64bit)에 따라 아래 명령어를 실헹

.. code:: bash

  pip install -U lxml\lxml-4.4.0-cp35-cp35m-win_amd64.whl


Unit Testing
==============================

.. code:: bash

  pytest

개별 파일 테스트

.. code:: bash

  pytest test_login.py


Syntax
======================


Basic
----------

.. code:: python

  from rs4.webkit import Chrome

  with Chrome (os.path.join (os.path.dirname (__file__), "drivers", "chromedriver"), headless = True) as driver:
    driver.navigate (URL)
    driver.wait_until ('visible', 'img[src="/assets/image/icon_home.png"]')
    btn_post = driver.one ("button-text:글쓰기")
    assert btn_post
    driver.capture ('capture.jpg')
    assert os.path.exists ('capture.jpg')


CSS Selector
---------------------

- All CSS
- button-text:TEXT
- link-text:TEXT


Driver Methods
--------------------

- one (CSS, until = 'presence', timeout = 5)
- fetch (CSS, until = 'presence', timeout = 5)
- as_select (CSS, until = 'presence', timeout = 5): return <select> as select object
- capture (save_path)
- sleep (timeout)
- wait (timeout): wait until page is loaded

- back ()
- forward ()
- refresh ()

- execute_script (javascript)
- execute_async_script (javascript)

- check (CHECKBOX_OR_RADIOBOX_LIST, INDEX_OR_vALUE)
- uncheck (CHECKBOX_OR_RADIOBOX_LIST, INDEX_OR_vALUE)
- check_all (CHECKBOX_OR_RADIOBOX_LIST)
- uncheck_all (CHECKBOX_OR_RADIOBOX_LIST)

- send_keys (CSS, TEXT)- select_by_index (CSS, INDEX)
- select_by_value (CSS, VALUE)
- deselect_by_index (CSS, INDEX)
- deselect_by_value (CSS, VALUE)
- select_all (CSS)
- deselect_all (CSS)

- get_cookie(name)
- get_cookies()
- add_cookie(cookie_dict)
- delete_all_cookies()
- delete_cookie(name)


Available Untils
-------------------------

- presence (default)
- loaded
- clickable
- visible
- invisible


Common Element Methods
-----------------------------

.. code:: python

  e = driver.one ('header')
  e.is_visible ()
  assert e.text.find ('마이페이지') != -1, "로그인 실패"

  inp = driver.one ('.srch-input')
  inp.send_keys ('zec\n')

- is_enabled ()
- is_displayed ()
- clear()
- click()
- text
- tag_name
- size
- submit ()
- send_keys (*value)


Select Methods
--------------------

.. code:: python

  sel = driver.as_select ('.srch-select')
  sel.select_by_index (1)

- select_by_index (INDEX)
- select_by_value (VALUE)
- deselect_by_index (INDEX)
- deselect_by_value (VALUE)
- select_all ()
- deselect_all ()
