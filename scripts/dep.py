#! /usr/bin/env python3

# sudo apt install awscli
# aws configure set aws_access_key_id ###########
# aws configure set aws_secret_access_key AKIA2B###########/dv3ELX1zWPZHzMMyS15OG
# aws configure set region ap-northeast-2
# aws configure set output json

import time
import sys, os
from rs4.apis.aws import ec2, deploy
import requests
from pprint import pprint
import math
from functools import partial; print = partial(print, flush = True); deploy.print = print
import binascii

# resources ------------------------------------------
INITIAL_AMI_ID = 'ami-0cd7b0de75f5a35d1'
SUBNETS = ['subnet-94c348d8', 'subnet-b76d8ecc']
SECURITY_GROUPS = ['sg-fa2bec90', 'sg-###########']
INSTANCE_ROLE_ARN = 'arn:aws:iam::###########:instance-profile/s3'
SSL_CERTIFICATION_ARN ='arn:aws:acm:ap-northeast-2:###########:certificate/1059e03d-1188-4170-b2cb-6da09ed7a13e'

# config ------------------------------------------
SYSTEM = 'example'
KEY_NAME = 'hardlock'
KEY_FILE = os.path.expanduser ('~/.aws/.aws-ap-northeast-2.pem')
AUTO_SCALING_RANGE = (1, 3)
USERDATA = [
    '#!/bin/bash',
    'cd /home/ubuntu',
    'git clone https://gitlab+deploy-token-156019:###########-e6@gitlab.com/###########/example.git',
    'cd example',
    'git checkout -b master && git pull origin master', # must do, defualt bramch is qa
    './scripts/install.sh --swap',
    "sed -i 's/<<COMMIT_SHA>>/{}/g' pwa/static/sw.js".format (os.environ ["CI_COMMIT_SHA"]),
    'rm /etc/nginx/sites-enabled/default',
    'ln -s /home/ubuntu/example/pwa/resources/nginx/nginx.conf /etc/nginx/sites-enabled/default',
    'systemctl reload nginx && systemctl status nginx',
    'chown -R ubuntu:ubuntu /home/ubuntu',
    'su - ubuntu -c "/usr/local/bin/skitai smtpda install"',
    'su - ubuntu -c "/home/ubuntu/example/serve.py install"',
    'systemctl start smtpda',
    'systemctl start example && systemctl status example'
]

def notify (msg):
    from telegram.error import TimedOut
    from rs4.apis import telegram

    bot = telegram.Telegram ('1071856030:###########-c')
    for i in range (3):
        try:
            bot.send (msg)
        except TimedOut:
            time.sleep (2)
            continue
        break

def testfunc (dep, inst):
    try:
        resp = requests.get ('http://{}'.format (inst.public_dns_name))
    except:
        return False
    if not (resp.status_code == 200 and 'mapVuexItems' in resp.text):
        return False
    with dep.ssh (inst, KEY_FILE) as ssh:
        if "Active: active (running)" not in ssh.run ('sudo systemctl status example', hide = True).stdout:
            return False
        if "Active: active (running)" not in ssh.run ('sudo systemctl status smtpda', hide = True).stdout:
            return False
    return True


if __name__ == '__main__':
    from rs4 import argopt
    argopt.add_option (None, '--id=INSTANCE_ID')
    argopt.add_option (None, '--clean')
    argopt.add_option ("-r", '--rollback')
    argopt.add_option ('-s=', '--strategy=STRATEGY', 'rescale|patch')
    argopt.add_option (None, '--system=SYSTEM_NAME')

    opts = argopt.collect ()
    if '--help' in opts:
        argopt.usage (True)
    if '--rollback' in opts:
        ans = input ('Are you sure rollback? (y/N) ')
        if not ans or ans not in ('yY'):
            print ('aborted.')
            sys.exit ()

    if "CI_COMMIT_SHA" not in os.environ:
        # run on command line
        os.environ ["CI_COMMIT_SHA"] = binascii.b2a_hex(os.urandom(16))

    dep = deploy.Deploy (SYSTEM, INITIAL_AMI_ID, KEY_NAME, SUBNETS, SECURITY_GROUPS)
    ISNTANCE_ID = opts.get ('--id')
    STRATEGY = opts.get ('--strategy')

    if not STRATEGY:
        commit_title = os.environ.get ("CI_COMMIT_TITLE")
        if not commit_title:
            print ('\nERROR: --strategy required')
            argopt.usage (False)
            sys.exit (1)
        STRATEGY = commit_title.find ('--hard-deploy') != -1 and 'rescale' or 'patch'

    if STRATEGY == 'rescale':
        if '--rollback' in opts:
            images = dep.get_images ()
            if len (images) < 2:
                print ('Error: no image to rollback'); sys.exit (3)
            image = ec2.Image (images [1]['ImageId'])
            dep.update_template (image)
        elif ISNTANCE_ID: # for simulating
            inst = ec2.Instance (ISNTANCE_ID)
            image = ec2.Image (dep.get_images () [0]['ImageId'])
        else:
            print ('launching temporary instance...')
            inst = dep.launch_instance ('t3.nano', USERDATA)
            image = dep.create_image (inst, testfunc)
            dep.update_template (image)
            inst.terminate ()

    if '--clean' in opts:
        dep.cleanup ()
        sys.exit ()

    dep.create_load_balancer_if_not_exists ()
    dep.create_target_group_if_not_exists ()
    dep.add_listener_if_not_exists (SSL_CERTIFICATION_ARN)
    dep.create_launch_template_if_not_exists ('t3.nano', INSTANCE_ROLE_ARN)
    dep.create_auto_scaling_group_if_not_exist (3, 16)
    dep.add_scaling_policy ('cpu-75', 'ASGAverageCPUUtilization', 75.0)
    dep.link_elb_to_domain ('example.co.kr')

    if STRATEGY == 'rescale':
        current_count = max (1, len ([ t ['Target']['Id'] for t in dep.get_elb_targets () if t ['TargetHealth']['State'] == 'healthy' ]))
        desired_count = max (1, math.ceil (current_count * 0.55))

        for i in range (3):
            print ('we want {} new versioning instances (session: {})'.format (desired_count, i + 1))
            new_instances = dep.rescale (image, current_count, desired_count)
            print ('created {} new version instances'.format (len (new_instances)))

            print ('waiting for starting instances draining...')
            time.sleep (10) #$ waiting for applying min, max policy
            for i in range (5):
                drainings = [ t ['Target']['Id'] for t in dep.get_elb_targets () if t ['TargetHealth']['State'] == 'draining' ]
                if drainings:
                    print ('{} old instances is on draining'.format (len (drainings)))
                    break
                time.sleep (30)

            current_count = len ([ t ['Target']['Id'] for t in dep.get_elb_targets () if t ['TargetHealth']['State'] == 'healthy' ])
            desired_count = len (dep.get_old_instances_by_image (image))
            if not desired_count:
                print ('old instances are completely removed')
                break

        if '--rollback' in opts:
            print ('removing lastest error image')
            dep.remove_image (dep.get_images () [0]['ImageId'])

        dep.set_auto_scaling_group_range (*AUTO_SCALING_RANGE)
        print ('autoscale range has been reinitialized.')

    elif STRATEGY == 'patch':
        patched = set ()
        for i in range (2):
            targets = dep.get_elb_targets ()
            print ('patching {} instances...'.format (len (targets)))
            for target in dep.get_elb_targets ():
                instance_id = target ['Target']['Id']
                if instance_id in patched:
                    print ('- instance {} was already patched, skip'.format (inst.id))
                    continue
                inst = ec2.Instance (instance_id)
                print ('- patching {}'.format (inst.id))
                with dep.ssh (inst, KEY_FILE, 'ubuntu') as ssh:
                    ssh.run ('cd example && git checkout -- pwa/static/sw.js')
                    if '--rollback' in opts:
                        ssh.run ('cd example && git checkout HEAD^')
                    else:
                        ssh.run ('cd example && git checkout master && git pull origin master')
                        ssh.run ('sudo pip3 install -r example/requirements.txt')
                        ssh.run ('sudo pip3 install -U atila rs4 skitai aquests sqlphile')
                        ssh.run ('sudo pip3 install -U atila rs4 skitai aquests sqlphile')
                    ssh.run ("sed -i 's/<<COMMIT_SHA>>/{}/g' example/pwa/static/sw.js".format (os.environ ["CI_COMMIT_SHA"]))
                    ssh.run ("sed -n '/-cache-/p' example/pwa/static/sw.js")
                    ssh.run ("sudo systemctl reload nginx")
                    ssh.run ("cd example && python3 ./serve.py --psychopath update")
                    ssh.run ("sudo systemctl restart example && sudo systemctl status example")
                patched.add (instance_id)
