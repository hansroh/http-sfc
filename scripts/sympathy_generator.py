#! /usr/bin/env python3

import sqlphile as sp
from rs4.psutil import service, daemon_class
import os
import rs4
import sys
import time
from rs4 import argopt, confparse
from rs4 import importer; settings = importer.from_file ('settings', os.path.join (os.path.dirname (__file__), '../pwa/orm/orm/settings.py'))
from datetime import datetime, timedelta, timezone

class Psycopath (daemon_class.DaemonClass):
    NAME = "psycopath"
    def __init__ (self, conn, logpath, varpath, consol):
        daemon_class.DaemonClass.__init__ (self, logpath, varpath, consol)
        self.conn = conn

    def session (self):
        with sp.pg2.open (*self.conn) as db:
            row = db.select ('configs').filter (item = 'sympathy_daemon').order_by ('-idx').limit (1).execute ().one ()
            cf = confparse.ConfParse ()
            cf.read_from_string (row.text)
            next_run = int (24 / cf.getint ('settings', 'run_per_day') * 60)
            if not cf.getboolean ('settings', 'enable'):
                self.log ('enable is false, do nothing in this ession')
                return next_run

            q = db.select ('contents').get ('idx, user_idx, sympathy, not_sympathy, reg_date_time').exclude (category_idx = 1).filter (status = 1)
            post_days_within = cf.getint ('settings', 'post-days-within')
            if post_days_within > 0:
                q.filter (reg_date_time__gt = datetime.utcnow () - timedelta (days = post_days_within))
            contents = q.execute ().fetch ()
            G = cf.getfloat ('settings', 'sympathy-factor')
            B = cf.getfloat ('settings', 'not-sympathy-factor')
            now = datetime.now (timezone.utc)
            TOTAL_SYM = 0
            TOTAL_NSYM = 0
            DIV = int (len (contents) / 5)
            for i, row in enumerate (contents):
                D = max (1, (now - row.reg_date_time).days)
                ADD_SYM = int (1 / D * G)
                TOTAL_SYM += ADD_SYM
                ADD_NSYM = int (1 / D * B)
                TOTAL_NSYM += ADD_NSYM
                SYM = row.sympathy + ADD_SYM
                NSYM = row.not_sympathy + ADD_NSYM
                q = db.with_ ('update_rewards',
                        db.update ('rewards')
                            .data (sympathy = sp.F ('sympathy + {}'.format (ADD_SYM)))
                            .data (not_sympathy = sp.F ('not_sympathy + {}'.format (ADD_NSYM)))
                            .data (amount = sp.F ('amount + {}'.format (ADD_SYM + ADD_NSYM)))
                            .filter (user_idx = row.user_idx))

                if i % DIV == 0:
                    act = '[무작위 샘플링] {}일전에 올린 글 {}에 공감 +{}/비공감 +{}, 현재 곻감 {}/비공감 {}'.format (D, row.idx, ADD_SYM, ADD_NSYM, SYM, NSYM)
                    q.with_ ('add_log', db.insert ('daemon_log').data (user_idx = row.user_idx, name = '공감데몬', act = act))
                q.update ('contents').data (sympathy = SYM, not_sympathy = NSYM).filter (idx = row.idx).execute ()
                db.commit ()

            act = '{}일내 게시글 {}개에 공감+{}/비공감+{}, 공/비설정: {}/{}, 다음실행: {}분후'.format (post_days_within, len (contents), TOTAL_SYM, TOTAL_NSYM, G, B, next_run)
            db.insert ('daemon_log').data (name = '공감데몬', act = act).execute ()
            db.commit ()

            return next_run

    def run (self):
        while self.active:
            try:
                sleep = self.session ()
            except KeyboardInterrupt:
                raise
            except:
                self.trace ()

            self.log ('sleep {} minutes'.format (sleep))
            for i in range (int (sleep * 60)):
                if not self.active:
                    break
                time.sleep (1)


def main ():
    # level2/test_eth_load.py
    opts = argopt.options ()

    _dbms = settings.DATABASES ["default"]
    CONN = (_dbms ["NAME"], _dbms ["USER"], _dbms ["PASSWORD"], _dbms ["HOST"], _dbms ["PORT"])

    log_dir = os.path.join ("/var/log/skitai", Psycopath.NAME)
    working_dir = os.path.join ("/var/tmp/skitai", Psycopath.NAME)
    servicer = service.Service ("example/{}".format (Psycopath.NAME), working_dir)
    try:
        cmd = opts.argv [0]
    except IndexError:
        cmd = None

    if cmd and not servicer.execute (cmd):
        return
    if not cmd and servicer.status (False):
        raise SystemError ("daemon is running")

    s = Psycopath (CONN, log_dir, working_dir, '-d' not in opts)
    s.start ()


if __name__ == "__main__":
    main ()
