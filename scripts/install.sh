#! /bin/bash

gitlabflag=no
swapflag=no
qaflag=no

if ! options=$(getopt -uo d: -l gitlab,swap,domain,qa: -- "$@")
then
    exit 1
fi
set -- $options
while [ $# -gt 0 ]
do
    case $1 in
    -d | --domain) domains+=($2); shift;;
    --qa) qaflag="yes";;
    --swap) swapflag="yes";;
    --gitlab) gitlabflag="yes";;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*) break;;
    esac
    shift
done

echo "gitlabflag=${gitlabflag}"
echo "swapflag=${swapflag}"
echo "domains=${domains[@]}"

domainlist="${domains[@]}"
for arg in ${domains[@]}
do
    certbot_domain_options+=" -d ${arg}"
done

apt update
DEBIAN_FRONTEND=noninteractive apt upgrade -yq

if id "ubuntu" >/dev/null 2>&1; then
    echo "user ubuntu exists"
else
    apt install -y sudo
    adduser --disabled-password --shell /bin/bash --gecos "ubuntu" ubuntu
    adduser ubuntu sudo
    echo 'ubuntu ALL=NOPASSWD: ALL' >> /etc/sudoers
    echo "user ubuntu created"
fi

apt install -y language-pack-ko
locale-gen ko_KR.UTF-8
update-locale LANG=ko_KR.UTF-8 LC_MESSAGES=POSIX

apt install -y git build-essential
apt install -y wget curl vim tmux python3 python3-dev python3-pip
apt install -y libpq5 postgresql-client-common postgresql-common zlib1g
apt install -y libgssapi-krb5-2 libldap-2.4-2 libssl-dev libpam0g libxml2
apt install -y locales ssl-cert sysstat git
apt install -y nginx
apt install -y software-properties-common

# sudo add-apt-repository ppa:deadsnakes/ppa
# sudo apt update && sudo apt install -y python3.6 python3.6-venv python3.6-dev
# sudo rm /usr/bin/python3
# sudo ln -s /usr/bin/python3.6 /usr/bin/python3
# wget https://bootstrap.pypa.io/get-pip.py && python3 get-pip.py

add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main"
wget -q -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

apt update && apt install -y postgresql-client-common postgresql-client-9.6

pip3 install -U pip setuptools
pip3 install -Ur requirements.txt

# make sure double install to avoid cahce effect
pip3 install -U --no-cache-dir atila rs4 skitai aquests sqlphile
pip3 install -U --no-cache-dir atila rs4 skitai aquests sqlphile

su - ubuntu -c "aws configure set region ap-northeast-2"
su - ubuntu -c "aws configure set output json"

! test -e /var/log/skitai && mkdir /var/log/skitai && chown -R ubuntu:ubuntu /var/log/skitai
! test -e /var/tmp/skitai && mkdir /var/tmp/skitai && chown -R ubuntu:ubuntu /var/tmp/skitai

if [ "$swapflag" = "yes" ]
then
    swapon -s
    fallocate -l 1G /.swapfile
    chmod 600 /.swapfile
    mkswap /.swapfile
    swapon /.swapfile
    swapon -s
    sh -c "echo '/.swapfile   none    swap    sw    0   0' >> /etc/fstab"
fi

if [ "$gitlabflag" = "yes" ]
then
    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
    apt install -y gitlab-runner

    gitlab-runner stop
    gitlab-runner uninstall
    gitlab-runner install --working-directory /home/ubuntu --user ubuntu
    gitlab-runner start
fi

if [ "$qaflag" = "yes" ]
then
    rm -f /etc/nginx/sites-enabled/example
    ln -s /home/ubuntu/example/resources/etc/nginx.qa.conf /etc/nginx/sites-enabled/example
fi
