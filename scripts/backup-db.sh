#sudo apt install postgresql-client-common
#sudo apt install postgresql-client-9.5

export PGPASSWORD="##########"
dbname="example_sandbox"

echo "backup to backend/resources/${dbname}.dump"
pg_dump -d ${dbname} -h 192.168.0.80 -U example -Ft > pwa/resources/${dbname}.dump

# echo "restore to backend/resources/${dbname}.dump"
#pg_restore --verbose -U example -d ${dbname} --role example -h 192.168.0.80 pwa/resources/${dbname}.dump
