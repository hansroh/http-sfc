schedflag=no
domain="qa.example.co.kr"
domain2=""

if ! options=$(getopt -uo s -l sched -- "$@")
then
    exit 1
fi
set -- $options
while [ $# -gt 0 ]
do
    case $1 in
    -s | --sched) schedflag="yes";;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*) break;;
    esac
    shift
done

add-apt-repository -y ppa:certbot/certbot
apt install -y nginx certbot python-certbot-nginx

cat <<EOF > /etc/nginx/sites-enabled/${domain}
include /home/ubuntu/example/pwa/resources/nginx/include/upstreams.conf;

server {
    listen 80;
    listen [::]:80;
    server_name ${domain} ${domain2};

    include /home/ubuntu/example/pwa/resources/nginx/include/header.conf;
    include /home/ubuntu/example/pwa/resources/nginx/include/route.conf;
}
EOF

if [ "$domain2" != "" ]
then
    certbot --nginx -n -m dwbaek@example.co.kr --redirect --agree-tos -d ${domain} -d ${domain2}
else
    certbot --nginx -n -m dwbaek@example.co.kr --redirect --agree-tos -d ${domain}
fi

if [ "$schedflag" = "yes" ]
then
    (crontab -l; echo "31 4,16 * * * /usr/bin/certbot renew") 2>&1 | grep -v "no crontab" | sort | uniq | crontab -
fi

rm /etc/nginx/sites-enabled/${domain}
ln -s /home/ubuntu/example/resources/etc/nginx.qa.ssl.conf /etc/nginx/sites-enabled/${domain}
